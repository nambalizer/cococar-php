<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Individualadvice extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('customer_model', 'customermodel');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
	}

	
	public function index()
	{
		$arrayData = array();
		$arrayData = $this->customermodel->retrieveAllNeedAdvice();
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-nav' => 0,
			'active-page' => '',
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/individualadvice/template.phtml', array(
            'display' => $display,
            'view' => 'templates/individualadvice/index',
            'viewjs' => 'templates/individualadvice/index-js'
        )); 
	}

	public function moreinformation()
	{
		$arrayData = array();
		$id = filter_var($this->input->get('id'), FILTER_SANITIZE_STRING);
		$arrayData = $this->customermodel->retrieveNeedAdvice($id);
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-nav' => 0,
			'active-page' => '',
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/individualadvice/template.phtml', array(
            'display' => $display,
            'view' => 'templates/individualadvice/moreinformation',
            'viewjs' => 'templates/individualadvice/index-js'
        )); 

	}
}