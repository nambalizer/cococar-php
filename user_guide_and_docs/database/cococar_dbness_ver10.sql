-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2016 at 06:24 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cococar_dbness`
--

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE IF NOT EXISTS `car` (
  `idcar` bigint(20) NOT NULL AUTO_INCREMENT,
  `make` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `engine` varchar(45) DEFAULT NULL,
  `modelvariant` varchar(45) DEFAULT NULL,
  `equipment` varchar(45) DEFAULT NULL,
  `generation` varchar(45) DEFAULT NULL,
  `body` varchar(45) DEFAULT NULL,
  `seats` varchar(45) DEFAULT NULL,
  `propulsion` varchar(45) DEFAULT NULL,
  `power` float DEFAULT NULL,
  `fuel` varchar(45) DEFAULT NULL,
  `fueltype` varchar(45) DEFAULT NULL,
  `displacement` varchar(45) DEFAULT NULL,
  `cylinder` int(11) DEFAULT NULL,
  `transmission` varchar(15) DEFAULT NULL,
  `dprice` float DEFAULT NULL,
  `chprice` float DEFAULT NULL,
  `safety` float DEFAULT NULL,
  `fuelcons` float DEFAULT NULL,
  `space` float DEFAULT NULL,
  `comfort` float DEFAULT NULL,
  `environment` float DEFAULT NULL,
  `powertrain` float DEFAULT NULL,
  `driving` float DEFAULT NULL,
  `handiness` float DEFAULT NULL,
  `reliability` float DEFAULT NULL,
  `multimedia` float DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  `car_makes_idcar_makes` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcar`),
  KEY `fk_car_car_makes1_idx` (`car_makes_idcar_makes`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='	' AUTO_INCREMENT=51 ;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`idcar`, `make`, `model`, `engine`, `modelvariant`, `equipment`, `generation`, `body`, `seats`, `propulsion`, `power`, `fuel`, `fueltype`, `displacement`, `cylinder`, `transmission`, `dprice`, `chprice`, `safety`, `fuelcons`, `space`, `comfort`, `environment`, `powertrain`, `driving`, `handiness`, `reliability`, `multimedia`, `isactive`, `car_makes_idcar_makes`) VALUES
(1, 'Volvo', 'XC90', 'D5 AWD', 'n.a.', 'Momentum', 'II', 'SUV', '5', 'AWD', 165, 'Diesel', 'Diesel', '1969', 4, 'A8', 54400, 71000, 3.51, 3.38, 3.83, 3.88, 1.98, 3.07, 3.57, 2.52, 3.17, 2.63, 1, 18),
(2, 'BMW', 'X5', 'xDrive 25d', 'n.a.', 'n.a.', 'III', 'SUV', '5', 'AWD', 160, 'Diesel', 'Diesel', '1995', 4, 'A8', 56900, 0, 3.26, 3.66, 3.67, 4, 1.98, 3.09, 3.57, 2.46, 3.01, 3.82, 1, 2),
(3, 'Volkswagen', 'Touareg', '3.0 V6 TDI SCR BlueMotion Technology 4MOTION', 'n.a.', 'n.a.', 'II FL1', 'SUV', '5', 'AWD', 150, 'Diesel', 'Diesel', '2967', 6, 'A8', 53050, 60250, 3.12, 3.45, 3.7, 4.3, 1.31, 2.37, 3.6, 2.75, 3.38, 3.48, 1, 17),
(4, 'Land Rover', 'Range Rover Sport', 'TDV6', 'n.a.', 'n.a.', 'II', 'SUV', '5', 'AWD', 190, 'Diesel', 'Diesel', '2993', 6, 'A8', 61900, 71300, 2.96, 3.51, 3.71, 3.85, 1.35, 3.69, 3.64, 2.91, 3.04, 3.1, 1, 8),
(5, 'Citro', 'C1', 'PureTech 82', 'n.a.', 'n.a.', 'II', 'Hatch5T', '4', 'FWD', 60, 'Benzin', 'Super', '1199', 3, 'M5', 11550, 17500, 2.16, 4.15, 1.52, 2.27, 3.63, 3.18, 2.65, 3.15, 2.85, 1.8, 1, 3),
(6, 'Hyundai', 'i10', 'Blue 1.0', 'n.a.', 'Trend', 'II', 'Hatch5T', '5', 'FWD', 49, 'Benzin', 'Super', '998', 3, 'M5', 10190, 12790, 1.86, 3.69, 1.59, 3.2, 3.38, 1.47, 2.87, 3.28, 4.16, 1.46, 1, 6),
(7, 'Opel', 'Karl', '1', 'n.a.', 'Exklusiv', 'I', 'Hatch5T', '4', 'FWD', 55, 'Benzin', 'Super', '999', 3, 'M5', 9500, 11950, 2.39, 3.33, 1.56, 3.13, 3.43, 1.57, 3.05, 3.27, 2.76, 2.35, 1, 12),
(8, 'Renault', 'Twingo', 'SCe70', 'n.a.', 'Luxe', 'III', 'Hatch5T', '4', 'RWD', 52, 'Benzin', 'Super', '999', 3, 'M5', 9690, 0, 2.07, 3.3, 1.38, 2.64, 3.64, 1.36, 2.37, 3.75, 2.82, 2.6, 1, 15),
(9, 'Volkswagen', 'up!', '1.0 BlueMotion Technology', 'n.a.', 'High Up', 'I', 'Hatch5T', '4', 'FWD', 55, 'Benzin', 'Super', '999', 3, 'M5', 11855, 15500, 2.17, 3.48, 1.68, 3.09, 3.59, 1.65, 3.1, 3.55, 2.95, 1.74, 1, 17),
(10, 'BMW', '1er', '118d', 'n.a.', 'n.a.', 'II FL1', 'Hatch5T', '5', 'RWD', 110, 'Diesel', 'Diesel', '1995', 4, 'A8', 28900, 0, 3.16, 4.32, 2.08, 3.73, 3.4, 3.67, 3.85, 2.99, 2.81, 3.5, 1, 2),
(11, 'Hyundai', 'i30', 'blue 1.6 CRDi', 'n.a.', 'n.a.', 'II FL1', 'Hatch5T', '5', 'FWD', 100, 'Diesel', 'Diesel', '1582', 4, 'DCT7', 22080, 0, 2.83, 4.3, 2.51, 3.66, 3.43, 3.29, 3.4, 2.36, 4.2, 2.53, 1, 6),
(12, 'Volkswagen', 'Golf', 'GTD', 'Variant', 'n.a.', 'VII', 'Kombi', '5', 'FWD', 135, 'Diesel', 'Diesel', '1968', 4, 'M6', 31975, 39600, 2.3, 3.51, 2.82, 3.93, 2.88, 2.91, 4.09, 2.86, 3.25, 4, 1, 17),
(13, 'Peugeot', '308', 'GT BlueHDI 180', 'SW', 'n.a.', 'II', 'Kombi', '5', 'FWD', 133, 'Diesel', 'Diesel', '1997', 4, 'A6', 33700, 40700, 1.83, 3.4, 2.57, 3.56, 2.93, 2.79, 3.71, 2.86, 3.06, 2.75, 1, 13),
(14, 'Ford', 'Focus', '2.0 TDCi', 'Turnier', 'ST', 'III FL1', 'Kombi', '5', 'FWD', 136, 'Diesel', 'Diesel', '1997', 4, 'M6', 31050, 37100, 2.2, 3.43, 2.37, 3.53, 2.96, 2.88, 3.83, 2.85, 2.78, 3, 1, 5),
(15, 'Skoda', 'Superb', '2.0 l TSI', 'n.a.', 'Style', 'III', 'Limousine', '5', 'FWD', 162, 'Benzin', 'Super', '1984', 4, 'DCT6', 34290, 0, 2.97, 2.78, 3.54, 3.95, 2.03, 3.62, 3.86, 2.29, 3.25, 3.48, 1, 16),
(16, 'Volkswagen', 'Passat', '2.0 TSI BlueMotion Technology', 'n.a.', 'Highline', 'VIII', 'Limousine', '5', 'FWD', 162, 'Benzin', 'Super', '1984', 4, 'DCT6', 37425, 43900, 3.38, 2.8, 3.14, 4.03, 2.08, 3.62, 3.88, 2.43, 3.32, 3.46, 1, 17),
(17, 'Citro', 'C4', 'Puretech 130', 'n.a.', 'Shine', 'II', 'Hatch5T', '5', 'FWD', 96, 'Benzin', 'Super', '1199', 3, 'A6', 22480, 0, 1.82, 3.08, 2, 3.41, 2.78, 1.77, 3.36, 2.6, 3.06, 2.25, 1, 3),
(18, 'Volkswagen', 'Golf', '1.4 TSI BlueMotion Technology', 'n.a.', 'Highline', 'VII', 'Hatch5T', '5', 'FWD', 92, 'Benzin', 'Super', '1395', 4, 'DCT7', 23375, 0, 3.29, 3.05, 2.41, 3.79, 2.85, 1.79, 3.7, 2.86, 3.28, 3.09, 1, 17),
(19, 'Jeep', 'Renegade', '1.6 MultiJet', 'n.a.', 'Limited', 'I', 'SUV', '5', 'FWD', 88, 'Diesel', 'Diesel', '1598', 4, 'M6', 23800, 25650, 2.81, 3.61, 2.46, 3.2, 2.97, 2.34, 3.17, 2.85, 2.74, 2.54, 1, 7),
(20, 'Mazda', 'CX-3', 'SKYACTIV-D 105', 'n.a.', 'Exclusive-Line', 'I', 'SUV', '5', 'FWD', 77, 'Diesel', 'Diesel', '1499', 4, 'M6', 21990, 28300, 2.83, 4.18, 1.92, 3, 3.34, 2.72, 3.41, 2.61, 3.32, 2.85, 1, 9),
(21, 'Mini', 'Countryman', 'Cooper D', 'n.a.', 'n.a.', 'I', 'SUV', '5', 'FWD', 82, 'Diesel', 'Diesel', '1598', 4, 'M6', 24450, 0, 2.18, 3.44, 1.72, 3.17, 3.02, 1.26, 3.56, 2.94, 2.88, 3, 1, 11),
(22, 'Porsche', 'Cayman', 'GTS', 'n.a.', 'n.a.', 'II', 'Coupe', '2', 'RWD', 250, 'Benzin', 'Super Plus', '3436', 6, 'DCT7', 77214, 0, 2.85, 2.02, 0.51, 3.22, 0.67, 4.09, 4.76, 2.28, 3.25, 2.75, 1, 14),
(23, 'Porsche', '911', 'Carrera GTS', 'n.a.', 'n.a.', 'VII', 'Coupe', '4', 'RWD', 316, 'Benzin', 'Super Plus', '3800', 6, 'DCT7', 117549, 0, 3.28, 1.76, 0.97, 3.15, 1.17, 4.74, 4.92, 2.51, 2.92, 2.91, 1, 14),
(24, 'BMW', '2er', '218d', 'Gran Tourer', 'Luxury Line', 'I', 'Van', '5', 'FWD', 110, 'Diesel', 'Diesel', '1995', 4, 'A8', 32350, 0, 3.27, 3.57, 3.3, 3.65, 3.08, 2.39, 4.01, 2.79, 3, 3.15, 1, 2),
(25, 'Opel', 'Zafira Tourer', '1.6 CDTI ecoFLEX', 'n.a.', 'Business Edition', 'III', 'Van', '5', 'FWD', 100, 'Diesel', 'Diesel', '1598', 4, 'M6', 26250, 0, 2.92, 3.56, 3.46, 3.81, 2.98, 1.28, 3.01, 2.43, 2.75, 2.69, 1, 12),
(26, 'BMW', '4er', '420i', 'n.a.', 'Luxury Line', 'I', 'Cabrio', '4', 'RWD', 135, 'Benzin', 'Super', '1997', 4, 'A8', 43900, 0, 2.3, 2.9, 1.72, 3.58, 1.65, 2.91, 3.53, 2.24, 2.88, 4.5, 1, 2),
(27, 'Opel', 'Cascada', '1.6 ECOTEC Direct Injection Turbo', 'n.a.', 'Innovation', 'I', 'Cabrio', '4', 'FWD', 147, 'Benzin', 'Super Plus', '1598', 4, 'M6', 30850, 39900, 1.95, 2.8, 1.51, 3.96, 1.51, 2.92, 2.7, 1.32, 2.88, 2.75, 1, 12),
(28, 'Audi', 'A1', '1.0 TFSI ultra', 'n.a.', 'Sport', 'I FL1', 'Hatch3T', '4', 'FWD', 70, 'Benzin', 'Super', '999', 3, 'M5', 17150, 22100, 2.35, 3.37, 1.55, 3.28, 3.45, 1.26, 3.53, 2.95, 3.12, 2.95, 1, 1),
(29, 'Mini', 'Mini', 'One', 'n.a.', 'n.a.', 'III', 'Hatch3T', '4', 'FWD', 75, 'Benzin', 'Super', '1198', 3, 'M6', 17650, 23800, 2.84, 3.16, 1.3, 3.19, 3.27, 2.68, 3.66, 3.13, 2.66, 2.94, 1, 11),
(30, 'Opel', 'Adam', '1.0 ECOTEC Direct Injection Turbo', 'n.a.', 'Slam', 'I', 'Hatch3T', '4', 'FWD', 85, 'Benzin', 'Super', '999', 3, 'M6', 16650, 18600, 2.33, 3.11, 1.17, 2.91, 3.05, 2.8, 3.53, 2.99, 2.6, 2.91, 1, 12),
(31, 'BMW', 'i3', 'n.a.', 'n.a.', 'n.a.', 'I', 'Hatch5T', '4', 'RWD', 125, 'Elektro', 'Elektro', 'n.a.', 0, 'n.a.', 34950, 0, 2.48, 2.19, 1.16, 3.51, 4.94, 2.55, 4.01, 3.41, 4.63, 4.63, 1, 2),
(32, 'Mercedes', 'B-Klasse', '250 e', 'n.a.', 'n.a.', 'II FL1', 'Hatch5T', '5', 'FWD', 132, 'Elektro', 'Elektro', 'n.a.', 0, 'n.a.', 39151, 0, 2.75, 2.21, 2.61, 3.73, 4.92, 2.47, 4, 2.74, 3.84, 3.75, 1, 10),
(33, 'Volkswagen', 'Golf', 'e-Golf', 'n.a.', 'n.a.', 'VII', 'Hatch5T', '5', 'FWD', 85, 'Elektro', 'Elektro', 'n.a.', 0, 'n.a.', 34900, 0, 3.29, 2.34, 2.41, 3.98, 4.96, 1.07, 3.85, 2.86, 3.28, 3.09, 1, 17),
(34, 'Audi', 'Q5', '2.0 TFSI quattro', 'n.a.', 'n.a.', 'I FL1', 'SUV', '5', 'AWD', 132, 'Benzin', 'Super', '1984', 4, 'M6', 39300, 49100, 3.12, 2.53, 3.18, 3.92, 1.15, 2.77, 3.01, 2.77, 3.38, 3.25, 1, 1),
(35, 'BMW', '5er', '520i', 'Touring', 'n.a.', 'VI FL1', 'Kombi', '5', 'RWD', 135, 'Benzin', 'Super', '1997', 4, 'A8', 43700, 0, 3.17, 2.4, 3.41, 4.08, 1.83, 2.32, 3.74, 2.24, 3.08, 3.95, 1, 2),
(36, 'Opel', 'Zafira Tourer', '1.6 ECOTEC Direct Injection Turbo ecoFLEX', 'n.a.', 'Style', 'III', 'Van', '5', 'FWD', 147, 'Benzin', 'Super Plus', '1598', 4, 'M6', 29205, 34000, 2.92, 2.16, 3.46, 3.61, 1.3, 2.89, 3.01, 2.43, 2.75, 2.69, 1, 12),
(37, 'Renault', 'Espace', 'ENERGY TCe 200 EDC', 'n.a.', 'Initiale Paris', 'V', 'Van', '5', 'FWD', 147, 'Benzin', 'Super', '1618', 4, 'DCT7', 38850, 41200, 2.89, 2.14, 3.56, 3.48, 2, 2.19, 2.91, 2.22, 3.63, 2.88, 1, 15),
(38, 'Audi', 'A7', '3.0 TDI competition quattro', 'Sportback', 'Quattro Competition', 'I FL1', 'Limousine', '4', 'AWD', 240, 'Diesel', 'Diesel', '2967', 6, 'A8', 73100, 92950, 3.35, 2.98, 3.09, 3.89, 1.45, 3.83, 4.34, 2.54, 3.43, 4.3, 1, 1),
(39, 'Porsche', 'Panamera', 'Diesel', 'n.a.', 'n.a.', 'I FL1', 'Limousine', '4', 'RWD', 221, 'Diesel', 'Diesel', '2967', 6, 'A8', 85300, 0, 3.48, 3.47, 3, 4.05, 1.28, 3.85, 4.19, 1.74, 3.1, 3.73, 1, 14),
(40, 'Fiat', '500X', '1.4 MultiAir', 'n.a.', 'Lounge', 'I', 'SUV', '5', 'FWD', 103, 'Benzin', 'Super', '1368', 4, 'M6', 21450, 24650, 2.42, 2.76, 2.07, 3.14, 2.23, 1.95, 3.44, 2.56, 2.53, 2.36, 1, 4),
(41, 'Renault', 'Captur', 'ENERGY TCe 120 EDC', 'n.a.', 'Luxe', 'I', 'SUV', '5', 'FWD', 88, 'Benzin', 'Super', '1197', 4, 'DCT6', 19890, 0, 1.36, 2.94, 1.72, 3.58, 2.67, 1.86, 2.55, 2.49, 2.69, 2.88, 1, 15),
(42, 'Ford', 'Mondeo', '2.0 TDCi', 'Turnier', 'Titanium', 'III', 'Kombi', '5', 'FWD', 132, 'Diesel', 'Diesel', '1997', 4, 'M6', 32450, 0, 3.26, 3.45, 2.95, 4.09, 2.72, 3.18, 3.79, 2.32, 2.88, 3.25, 1, 5),
(43, 'Mazda', '6', 'SKYACTIV-D 175 i-ELOOP', 'Kombi', 'Sports-Line', 'III', 'Kombi', '5', 'FWD', 129, 'Diesel', 'Diesel', '2191', 4, 'M6', 36790, 0, 1.77, 3.5, 2.79, 3.6, 2.68, 3.57, 3.87, 1.76, 3.69, 3.25, 1, 9),
(44, 'Opel', 'Insignia', '2.0 CDTI ecoFLEX', 'Sports Tourer', 'Business Edition', 'I FL1', 'Kombi', '5', 'FWD', 125, 'Diesel', 'Diesel', '1956', 4, 'M6', 33185, 39050, 2.52, 3.69, 2.65, 3.71, 2.69, 3.47, 3.27, 2.03, 3.06, 2, 1, 12),
(45, 'Mercedes', 'E-Klasse', '220 BlueTEC', 'T-Modell', 'Elegance', 'IV FL1', 'Kombi', '5', 'RWD', 125, 'Diesel', 'Diesel', '2143', 4, 'A9', 50158.5, 67300, 3.31, 3.6, 3.55, 4.24, 2.76, 3.03, 3.41, 2.46, 3.33, 3.7, 1, 10),
(46, 'Volvo', 'V70', 'D4', 'n.a.', 'Momentum', 'III FL1', 'Kombi', '5', 'FWD', 133, 'Diesel', 'Diesel', '1969', 4, 'A8', 41450, 0, 2.59, 3.44, 2.79, 3.73, 2.65, 2.89, 3.21, 2.2, 3.06, 2.75, 1, 18),
(47, 'Audi', 'Q7', '3.0 TDI quattro', 'n.a.', 'n.a.', 'II', 'SUV', '5', 'AWD', 200, 'Diesel', 'Diesel', '2967', 6, 'A8', 60900, 77850, 3.5, 3.37, 4.29, 4.34, 2.02, 3.55, 3.9, 2.67, 3.44, 3.85, 1, 1),
(48, 'BMW', '5er', '520d', 'n.a.', 'n.a.', 'VI FL1', 'Limousine', '5', 'RWD', 140, 'Diesel', 'Diesel', '1995', 4, 'A8', 42900, 0, 3.06, 3.72, 2.98, 4.14, 2.94, 2.92, 4.1, 2.25, 3.12, 4.03, 1, 2),
(49, 'Skoda', 'Superb', '2.0 l TDI SCR', 'n.a.', 'Style', 'III', 'Limousine', '5', 'FWD', 140, 'Diesel', 'Diesel', '1968', 4, 'DCT6', 32290, 0, 2.97, 4.12, 3.54, 3.8, 2.98, 3.13, 3.75, 2.29, 3.25, 3.48, 1, 16),
(50, 'Mercedes', 'C-Klasse', '400 4MATIC', 'n.a.', 'Exlcusive', 'IV', 'Limousine', '5', 'AWD', 245, 'Benzin', 'Super', '2996', 6, 'A7', 52389.8, 65200, 3.52, 2.66, 2.65, 3.97, 1.48, 4.04, 3.89, 2.56, 3.34, 3.53, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `car_has_car_picture`
--

CREATE TABLE IF NOT EXISTS `car_has_car_picture` (
  `idcar_has_car_picture` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_idcar` bigint(20) NOT NULL,
  `car_picture_idcar_picture` bigint(20) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcar_has_car_picture`),
  KEY `fk_car_has_car_picture_car_picture1_idx` (`car_picture_idcar_picture`),
  KEY `fk_car_has_car_picture_car1_idx` (`car_idcar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `car_has_car_picture`
--

INSERT INTO `car_has_car_picture` (`idcar_has_car_picture`, `car_idcar`, `car_picture_idcar_picture`, `isactive`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 3, 3, 1),
(4, 4, 4, 1),
(5, 5, 5, 1),
(6, 6, 6, 1),
(7, 7, 7, 1),
(8, 8, 8, 1),
(9, 9, 9, 1),
(10, 10, 10, 1),
(11, 11, 11, 1),
(12, 12, 12, 1),
(13, 13, 13, 1),
(14, 14, 14, 1),
(15, 15, 15, 1),
(16, 16, 16, 1),
(17, 17, 17, 1),
(18, 18, 18, 1),
(19, 19, 19, 1),
(20, 20, 20, 1),
(21, 21, 21, 1),
(22, 22, 22, 1),
(23, 23, 23, 1),
(24, 24, 24, 1),
(25, 25, 25, 1),
(26, 26, 26, 1),
(27, 27, 27, 1),
(28, 28, 28, 1),
(29, 29, 29, 1),
(30, 30, 30, 1),
(31, 31, 31, 1),
(32, 32, 32, 1),
(33, 33, 33, 1),
(34, 34, 34, 1),
(35, 35, 35, 1),
(36, 36, 36, 1),
(37, 37, 37, 1),
(38, 38, 38, 1),
(39, 39, 39, 1),
(40, 40, 40, 1),
(41, 41, 41, 1),
(42, 42, 42, 1),
(43, 43, 43, 1),
(44, 44, 44, 1),
(45, 45, 45, 1),
(46, 46, 46, 1),
(47, 47, 47, 1),
(48, 48, 48, 1),
(49, 49, 49, 1),
(50, 50, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_makes`
--

CREATE TABLE IF NOT EXISTS `car_makes` (
  `idcar_makes` bigint(20) NOT NULL AUTO_INCREMENT,
  `maker` varchar(45) DEFAULT NULL,
  `iconurl` varchar(45) DEFAULT NULL,
  `logourl` varchar(45) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcar_makes`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `car_makes`
--

INSERT INTO `car_makes` (`idcar_makes`, `maker`, `iconurl`, `logourl`, `isactive`) VALUES
(1, 'Audi', 'icon-audi.png', 'logo-audi.png', 1),
(2, 'BMW', 'icon-bmw.png', 'logo-bmw.png', 1),
(3, 'Citro', 'icon-citro.png', 'logo-citro.png', 1),
(4, 'Fiat', 'icon-fiat.png', 'logo-fiat.png', 1),
(5, 'Ford', 'icon-ford.png', 'logo-ford.png', 1),
(6, 'Hyundai', 'icon-hyundai.png', 'logo-hyundai.png', 1),
(7, 'Jeep', 'icon-jeep.png', 'logo-jeep.png', 1),
(8, 'Land Rover', 'icon-landrover.png', 'logo-landrover.png', 1),
(9, 'Mazda', 'icon-mazda.png', 'logo-mazda.png', 1),
(10, 'Mercedes', 'icon-mercedes.png', 'logo-mercedes.png', 1),
(11, 'Mini', 'icon-mini.png', 'logo-mini.png', 1),
(12, 'Opel', 'icon-opel.png', 'logo-opel.png', 1),
(13, 'Peugeot', 'icon-peugeot.png', 'logo-peugeot.png', 1),
(14, 'Porsche', 'icon-porsche.png', 'logo-porsche.png', 1),
(15, 'Renault', 'icon-renault.png', 'logo-renault.png', 1),
(16, 'Skoda', 'icon-skoda.png', 'logo-skoda.png', 1),
(17, 'Volkswagen', 'icon-volkswagen.png', 'logo-volkswagen.png', 1),
(18, 'Volvo', 'icon-volvo.png', 'logo-volvo.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_model_icon`
--

CREATE TABLE IF NOT EXISTS `car_model_icon` (
  `idcar_makes` bigint(20) NOT NULL AUTO_INCREMENT,
  `model` varchar(45) DEFAULT NULL,
  `iconurl` varchar(45) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcar_makes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_model_icon_has_car`
--

CREATE TABLE IF NOT EXISTS `car_model_icon_has_car` (
  `idcar_model_icon_has_car` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_model_icon_idcar_makes` bigint(20) NOT NULL,
  `car_idcar` bigint(20) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcar_model_icon_has_car`),
  KEY `fk_car_model_icon_has_car_car1_idx` (`car_idcar`),
  KEY `fk_car_model_icon_has_car_car_model_icon1_idx` (`car_model_icon_idcar_makes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_picture`
--

CREATE TABLE IF NOT EXISTS `car_picture` (
  `idcar_picture` bigint(20) NOT NULL AUTO_INCREMENT,
  `pictureurl` varchar(45) DEFAULT NULL,
  `iconurl` varchar(45) DEFAULT NULL,
  `fordesign` tinyint(1) DEFAULT '1',
  `isactive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcar_picture`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `car_picture`
--

INSERT INTO `car_picture` (`idcar_picture`, `pictureurl`, `iconurl`, `fordesign`, `isactive`) VALUES
(1, 'cars-1.png', 'icon-cars-1.png', 1, 1),
(2, 'cars-2.png', 'icon-cars-2.png', 1, 1),
(3, 'cars-3.png', 'icon-cars-3.png', 1, 1),
(4, 'cars-4.png', 'icon-cars-4.png', 1, 1),
(5, 'cars-5.png', 'icon-cars-5.png', 1, 1),
(6, 'cars-6.png', 'icon-cars-6.png', 1, 1),
(7, 'cars-7.png', 'icon-cars-7.png', 1, 1),
(8, 'cars-8.png', 'icon-cars-8.png', 1, 1),
(9, 'cars-9.png', 'icon-cars-9.png', 1, 1),
(10, 'cars-10.png', 'icon-cars-10.png', 1, 1),
(11, 'cars-11.png', 'icon-cars-11.png', 1, 1),
(12, 'cars-12-18-33.png', 'icon-cars-12-18-33.png', 1, 1),
(13, 'cars-13.png', 'icon-cars-13.png', 1, 1),
(14, 'cars-14.png', 'icon-cars-14.png', 1, 1),
(15, 'cars-15and49.png', 'icon-cars-15and49.png', 1, 1),
(16, 'cars-16.png', 'icon-cars-16.png', 1, 1),
(17, 'cars-17.png', 'icon-cars-17.png', 1, 1),
(18, 'cars-12-18-33.png', 'icon-cars-12-18-33.png', 1, 1),
(19, 'cars-19.png', 'icon-cars-19.png', 1, 1),
(20, 'cars-20.png', 'icon-cars-20.png', 1, 1),
(21, 'cars-21.png', 'icon-cars-21.png', 1, 1),
(22, 'cars-22.png', 'icon-cars-22.png', 1, 1),
(23, 'cars-23.png', 'icon-cars-23.png', 1, 1),
(24, 'cars-24.png', 'icon-cars-24.png', 1, 1),
(25, 'cars-25.png', 'icon-cars-25.png', 1, 1),
(26, 'cars-26.png', 'icon-cars-26.png', 1, 1),
(27, 'cars-27.png', 'icon-cars-27.png', 1, 1),
(28, 'cars-28.png', 'icon-cars-28.png', 1, 1),
(29, 'cars-29.png', 'icon-cars-29.png', 1, 1),
(30, 'cars-30.png', 'icon-cars-30.png', 1, 1),
(31, 'cars-31.png', 'icon-cars-31.png', 1, 1),
(32, 'cars-32.png', 'icon-cars-32.png', 1, 1),
(33, 'cars-12-18-33.png', 'icon-cars-12-18-33.png', 1, 1),
(34, 'cars-34.png', 'icon-cars-34.png', 1, 1),
(35, 'cars-35and48.png', 'icon-cars-35and48.png', 1, 1),
(36, 'cars-36.png', 'icon-cars-36.png', 1, 1),
(37, 'cars-37.png', 'icon-cars-37.png', 1, 1),
(38, 'cars-38.png', 'icon-cars-38.png', 1, 1),
(39, 'cars-39.png', 'icon-cars-39.png', 1, 1),
(40, 'cars-40.png', 'icon-cars-40.png', 1, 1),
(41, 'cars-41.png', 'icon-cars-41.png', 1, 1),
(42, 'cars-42.png', 'icon-cars-42.png', 1, 1),
(43, 'cars-43.png', 'icon-cars-43.png', 1, 1),
(44, 'cars-44.png', 'icon-cars-44.png', 1, 1),
(45, 'cars-45.png', 'icon-cars-45.png', 1, 1),
(46, 'cars-46.png', 'icon-cars-46.png', 1, 1),
(47, 'cars-47.png', 'icon-cars-47.png', 1, 1),
(48, 'cars-35and48.png', 'icon-cars-35and48.png', 1, 1),
(49, 'cars-15and49.png', 'icon-cars-15and49.png', 1, 1),
(50, 'cars-50.png', 'icon-cars-50.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_advice`
--

CREATE TABLE IF NOT EXISTS `customer_advice` (
  `idcustomer_advice` int(11) NOT NULL AUTO_INCREMENT,
  `customer_information_idcustomer_information` bigint(20) NOT NULL,
  `current` tinyint(1) DEFAULT NULL,
  `dateset` datetime DEFAULT NULL,
  PRIMARY KEY (`idcustomer_advice`),
  KEY `fk_customer_advice_customer_information1_idx` (`customer_information_idcustomer_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_appointment`
--

CREATE TABLE IF NOT EXISTS `customer_appointment` (
  `idcustomer_appointment` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_information_idcustomer_information` bigint(20) NOT NULL,
  `current` tinyint(1) DEFAULT NULL,
  `appointmentdate` datetime DEFAULT NULL,
  `dateset` datetime DEFAULT NULL,
  PRIMARY KEY (`idcustomer_appointment`),
  KEY `fk_customer_appointment_customer_information1_idx` (`customer_information_idcustomer_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_appointment_has_car`
--

CREATE TABLE IF NOT EXISTS `customer_appointment_has_car` (
  `customer_appointment_has_carid` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_appointment_idcustomer_appointment` bigint(20) NOT NULL,
  `car_idcar` bigint(20) NOT NULL,
  `percentage` float DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`customer_appointment_has_carid`),
  KEY `fk_customer_appointment_has_car_car1_idx` (`car_idcar`),
  KEY `fk_customer_appointment_has_car_customer_appointment1_idx` (`customer_appointment_idcustomer_appointment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_information`
--

CREATE TABLE IF NOT EXISTS `customer_information` (
  `idcustomer_information` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateofaction` datetime DEFAULT NULL,
  `fullname` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`idcustomer_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_makes`
--

CREATE TABLE IF NOT EXISTS `customer_makes` (
  `idcustomer_makes` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_information_idcustomer_information` bigint(20) NOT NULL,
  `car_makes_idcar_makes` bigint(20) NOT NULL,
  `isactive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idcustomer_makes`),
  KEY `fk_customer_makes_customer_information1_idx` (`customer_information_idcustomer_information`),
  KEY `fk_customer_makes_car_makes1_idx` (`car_makes_idcar_makes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_options`
--

CREATE TABLE IF NOT EXISTS `customer_options` (
  `coid` bigint(20) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,0) DEFAULT NULL,
  `passengers` int(11) DEFAULT NULL,
  `make` varchar(150) DEFAULT NULL,
  `model` varchar(150) DEFAULT NULL,
  `bodytype` varchar(150) DEFAULT NULL,
  `generation` varchar(150) DEFAULT NULL,
  `safety` int(11) DEFAULT NULL,
  `fuelconsumption` int(11) DEFAULT NULL,
  `space` int(11) DEFAULT NULL,
  `comfort` int(11) DEFAULT NULL,
  `powertrain` int(11) DEFAULT NULL,
  `handiness` int(11) DEFAULT NULL,
  `reliability` int(11) DEFAULT NULL,
  `multimedia` int(11) DEFAULT NULL,
  `dateofaction` datetime NOT NULL,
  `current` tinyint(1) NOT NULL,
  `customer_information_idcustomer_information` bigint(20) NOT NULL,
  PRIMARY KEY (`coid`),
  KEY `fk_customer_options_customer_information1_idx` (`customer_information_idcustomer_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_yourdesign`
--

CREATE TABLE IF NOT EXISTS `customer_yourdesign` (
  `idcustomer_yourdesign` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_idcar` bigint(20) NOT NULL,
  `percentage` float DEFAULT NULL,
  `like` tinyint(1) NOT NULL,
  `customer_information_idcustomer_information` bigint(20) NOT NULL,
  PRIMARY KEY (`idcustomer_yourdesign`),
  KEY `fk_customer_yourdesign_car_idx` (`car_idcar`),
  KEY `fk_customer_yourdesign_customer_information1_idx` (`customer_information_idcustomer_information`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `car`
--
ALTER TABLE `car`
  ADD CONSTRAINT `fk_car_car_makes1` FOREIGN KEY (`car_makes_idcar_makes`) REFERENCES `car_makes` (`idcar_makes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `car_has_car_picture`
--
ALTER TABLE `car_has_car_picture`
  ADD CONSTRAINT `fk_car_has_car_picture_car1` FOREIGN KEY (`car_idcar`) REFERENCES `car` (`idcar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_car_has_car_picture_car_picture1` FOREIGN KEY (`car_picture_idcar_picture`) REFERENCES `car_picture` (`idcar_picture`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `car_model_icon_has_car`
--
ALTER TABLE `car_model_icon_has_car`
  ADD CONSTRAINT `fk_car_model_icon_has_car_car1` FOREIGN KEY (`car_idcar`) REFERENCES `car` (`idcar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_car_model_icon_has_car_car_model_icon1` FOREIGN KEY (`car_model_icon_idcar_makes`) REFERENCES `car_model_icon` (`idcar_makes`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_advice`
--
ALTER TABLE `customer_advice`
  ADD CONSTRAINT `fk_customer_advice_customer_information1` FOREIGN KEY (`customer_information_idcustomer_information`) REFERENCES `customer_information` (`idcustomer_information`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_appointment`
--
ALTER TABLE `customer_appointment`
  ADD CONSTRAINT `fk_customer_appointment_customer_information1` FOREIGN KEY (`customer_information_idcustomer_information`) REFERENCES `customer_information` (`idcustomer_information`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_appointment_has_car`
--
ALTER TABLE `customer_appointment_has_car`
  ADD CONSTRAINT `fk_customer_appointment_has_car_car1` FOREIGN KEY (`car_idcar`) REFERENCES `car` (`idcar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_appointment_has_car_customer_appointment1` FOREIGN KEY (`customer_appointment_idcustomer_appointment`) REFERENCES `customer_appointment` (`idcustomer_appointment`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_makes`
--
ALTER TABLE `customer_makes`
  ADD CONSTRAINT `fk_customer_makes_car_makes1` FOREIGN KEY (`car_makes_idcar_makes`) REFERENCES `car_makes` (`idcar_makes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_makes_customer_information1` FOREIGN KEY (`customer_information_idcustomer_information`) REFERENCES `customer_information` (`idcustomer_information`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_options`
--
ALTER TABLE `customer_options`
  ADD CONSTRAINT `fk_customer_options_customer_information1` FOREIGN KEY (`customer_information_idcustomer_information`) REFERENCES `customer_information` (`idcustomer_information`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_yourdesign`
--
ALTER TABLE `customer_yourdesign`
  ADD CONSTRAINT `fk_customer_yourdesign_car` FOREIGN KEY (`car_idcar`) REFERENCES `car` (`idcar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_yourdesign_customer_information1` FOREIGN KEY (`customer_information_idcustomer_information`) REFERENCES `customer_information` (`idcustomer_information`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
