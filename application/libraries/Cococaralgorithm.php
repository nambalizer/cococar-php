<?php 
/*
	Cococar algorithm for dream car PHP Library (CodeIgniter Version)
	/////////////////////////////////
	This is a very simple Cococar algorithm.

		$this->load->library('cococaralgorithm');
		$this->cococaralgorithm->populateFromDb($arrayDb);
		$this->cococaralgorithm->calculateAllCoefficients($arrayDesired);
		$this->cococaralgorithm->returnArrayFit();

	Copyright 2016, Nambal.com inc and Cococar LTd.

	Contributors:
		+ Cococar team

	Converted to CI library by:
		+ Edison Quinones (http://www.nambal.com)

*/

class Cococaralgorithm {
	/////////////////////////////////////////////////
	// PROPERTIES, PRIVATE AND PROTECTED
	/////////////////////////////////////////////////
	
	/**
	 * Desired price of the car.
	 * @var float
	 * @access private
	 */
	private $uiPrice = '';

	/**
	 * Number of passengers.
	 * @var int
	 * @access private
	 */
	private $uiPassenger = '';

	/**
	 * Desired number of safety rate.
	 * @var float
	 * @access private
	 */
	private $safety_des;	
	/**
	 * Desired number of Fuel conservation rate.
	 * @var float
	 * @access private
	 */
	private $fuel_cons_des;
	/**
	 * Desired number of space rate.
	 * @var float
	 * @access private
	 */
	private $space_des;
	/**
	 * Desired number of comfort rate.
	 * @var float
	 * @access private
	 */
	private $comfort_des;
	/**
	 * Desired number of power train rate.
	 * @var float
	 * @access private
	 */
	private $powertrain_des;
	/**
	 * Desired number of handling rate.
	 * @var float
	 * @access private
	 */
	private $handiness_des;
	/**
	 * Desired number of reliability rate.
	 * @var float
	 * @access private
	 */
	private $reliability_des;
	/**
	 * Desired number of multimedia rate.
	 * @var float
	 * @access private
	 */
	private $multimedia_des;

	/**
	 * Populate from DB number of safety rate.
	 * @var float
	 * @access private
	 */
	private $safety_db;
	/**
	 * Populate from DB number of fuel_cons rate.
	 * @var float
	 * @access private
	 */
	private $fuel_cons_db;
	/**
	 * Populate from DB number of space rate.
	 * @var float
	 * @access private
	 */
	private $space_db;
	/**
	 * Populate from DB number of comfort rate.
	 * @var float
	 * @access private
	 */
	private $comfort_db;
	/**
	 * Populate from DB number of powertrain rate.
	 * @var float
	 * @access private
	 */
	private $powertrain_db;
	/**
	 * Populate from DB number of handiness rate.
	 * @var float
	 * @access private
	 */
	private $handiness_db;
	/**
	 * Populate from DB number of reliability rate.
	 * @var float
	 * @access private
	 */
	private $reliability_db;
	/**
	 * Populate from DB number of multimedia rate.
	 * @var float
	 * @access private
	 */
	private $multimedia_db;

	/**
	 * Populate from DB number in array.
	 * @var array
	 * @access private
	 */
	private $arrayDb = array();

	/**
	 * Populate array from results of fit coefficients.
	 * @var array
	 * @access private
	 */
	private $arrayFit = array();

	/**
	 * Constructor
	 * @param boolean $exceptions Should we throw external exceptions?
	 */
	public function __construct($exceptions = false) 
	{
		$this->exceptions = ($exceptions == true);
	}

	/**
	 * calculate fit coefficients safety_des
	 * @param float $safety_des
	 * @return float coefficients for safety_fit.
	 */
	public function calculatesafety($safety_des)
	{
		$this->safety_des = $safety_des;
		if ($this->safety_db < $this->safety_des) {
			$safety_fit = pow((min($this->safety_db, $this->safety_des)/max($this->safety_db,$this->safety_des)),2);
		} else {
			$safety_fit = min($this->safety_db, $this->safety_des)/max($this->safety_db, $safety_des);
		}

		return $safety_fit;
	}
	/**
	 * calculate fit coefficients fuel_cons_des
	 * @param float $fuel_cons_des
	 * @return float coefficients for fuel_cons_fit.
	 */
	public function calculatefuelcons($fuel_cons_des)
	{
		$this->fuel_cons_des = $fuel_cons_des;
		if ($this->fuel_cons_db < $this->fuel_cons_des) {
			$fuel_cons_fit = pow((min($this->fuel_cons_db, $this->fuel_cons_des)/max($this->fuel_cons_db,$this->fuel_cons_des)),2);
		} else {
			$fuel_cons_fit = min($this->fuel_cons_db, $this->fuel_cons_des)/max($this->fuel_cons_db, $fuel_cons_des);
		}

		return $fuel_cons_fit;
	}
	/**
	 * calculate fit coefficients space_des
	 * @param float $space_des
	 * @return float coefficients for space_fit.
	 */
	public function calculatespace($space_des)
	{
		$this->space_des = $space_des;
		if ($this->space_db < $this->space_des) {
			$space_fit = pow((min($this->space_db, $this->space_des)/max($this->space_db,$this->space_des)),2);
		} else {
			$space_fit = min($this->space_db, $this->space_des)/max($this->space_db, $space_des);
		}

		return $space_fit;
	}
	/**
	 * calculate fit coefficients comfort_des
	 * @param float $comfort_des
	 * @return float coefficients for comfort_fit.
	 */
	public function calculatecomfort($comfort_des)
	{
		$this->comfort_des = $comfort_des;
		if ($this->comfort_db < $this->comfort_des) {
			$comfort_fit = pow((min($this->comfort_db, $this->comfort_des)/max($this->comfort_db,$this->comfort_des)),2);
		} else {
			$comfort_fit = min($this->comfort_db, $this->comfort_des)/max($this->comfort_db, $comfort_des);
		}

		return $comfort_fit;
	}
	/**
	 * calculate fit coefficients powertrain_des
	 * @param float $powertrain_des
	 * @return float coefficients for powertrain_fit.
	 */
	public function calculatepowertrain($powertrain_des)
	{
		$this->powertrain_des = $powertrain_des;
		if ($this->powertrain_db < $this->powertrain_des) {
			$powertrain_fit = pow((min($this->powertrain_db, $this->powertrain_des)/max($this->powertrain_db,$this->powertrain_des)),2);
		} else {
			$powertrain_fit = min($this->powertrain_db, $this->powertrain_des)/max($this->powertrain_db, $powertrain_des);
		}

		return $powertrain_fit;
	}
	/**
	 * calculate fit coefficients handiness_des
	 * @param float $handiness_des
	 * @return float coefficients for handiness_fit.
	 */
	public function calculatehandiness($handiness_des)
	{
		$this->handiness_des = $handiness_des;
		if ($this->handiness_db < $this->handiness_des) {
			$handiness_fit = pow((min($this->handiness_db, $this->handiness_des)/max($this->handiness_db,$this->handiness_des)),2);
		} else {
			$handiness_fit = min($this->handiness_db, $this->handiness_des)/max($this->handiness_db, $handiness_des);
		}

		return $handiness_fit;
	}
	/**
	 * calculate fit coefficients reliability_des
	 * @param float $reliability_des
	 * @return float coefficients for reliability_fit.
	 */
	public function calculatereliability($reliability_des)
	{
		$this->reliability_des = $reliability_des;
		if ($this->reliability_db < $this->reliability_des) {
			$reliability_fit = pow((min($this->reliability_db, $this->reliability_des)/max($this->reliability_db,$this->reliability_des)),2);
		} else {
			$reliability_fit = min($this->reliability_db, $this->reliability_des)/max($this->reliability_db, $reliability_des);
		}

		return $reliability_fit;
	}
	/**
	 * calculate fit coefficients multimedia_des
	 * @param float $multimedia_des
	 * @return float coefficients for multimedia_fit.
	 */
	public function calculatemultimedia($multimedia_des)
	{
		$this->multimedia_des = $multimedia_des;
		if ($this->multimedia_db < $this->multimedia_des) {
			$multimedia_fit = pow((min($this->multimedia_db, $this->multimedia_des)/max($this->multimedia_db,$this->multimedia_des)),2);
		} else {
			$multimedia_fit = min($this->multimedia_db, $this->multimedia_des)/max($this->multimedia_db, $multimedia_des);
		}

		return $multimedia_fit;
	}


	/**
	 * Populate the db variables
	 * safety_db,
	 * fuel_cons_db,
	 * space_db,
	 * comfort_db,
	 * powertrain_db,
	 * handniness_db,
	 * reliability_db,
	 * multimedia_db
	 * example: $arrayDb = Array('safety_db' => 1, 'fuel_cons_db' => 5 ... );
	 * @param array $arrayDb
	 * @return boolean true on success, false if not succesful :P
	 */
	public function populateFromDb($arrayDb)
	{
		$this->safety_db = $arrayDb['safety_db'];
		$this->fuel_cons_db = $arrayDb['fuel_cons_db'];
		$this->space_db = $arrayDb['space_db'];
		$this->comfort_db = $arrayDb['comfort_db'];
		$this->powertrain_db = $arrayDb['powertrain_db'];
		$this->handiness_db = $arrayDb['handniness_db'];
		$this->reliability_db = $arrayDb['reliability_db'];
		$this->multimedia_db = $arrayDb['multimedia_db'];

		return true;
	}

	/**
	 * calculate all fit coefficients
	 * safety_desired
	 * fuel_cons_desired
	 * space_desired
	 * comfort_desired
	 * powertrain_desired
	 * handiness_desired
	 * reliability_desired
	 * multimedia_desired
	 * example: $arrayDesired = array('safety_desired' => 1, 'fuel_cons_desired' => 2)
	 * @param float $arrayDesired
	 * @return float coefficients for a car.
	 */
	public function calculateAllCoefficients($arrayDesired)
	{
		$safety_fit = $this->calculatesafety($arrayDesired['safety_desired']);
		$fuel_cons_fit = $this->calculatefuelcons($arrayDesired['fuel_cons_desired']);
		$space_fit = $this->calculatespace($arrayDesired['space_desired']);
		$comfort_fit = $this->calculatecomfort($arrayDesired['comfort_desired']);
		$powertrain_fit = $this->calculatepowertrain($arrayDesired['powertrain_desired']);
		$handiness_fit = $this->calculatehandiness($arrayDesired['handiness_desired']);
		$reliability_fit = $this->calculatereliability($arrayDesired['reliability_desired']);
		$multimedia_fit = $this->calculatemultimedia($arrayDesired['multimedia_desired']);

		$this->arrayFit = array(
			'safety_fit' => $safety_fit,
			'fuel_cons_fit' => $fuel_cons_fit,
			'space_fit' => $space_fit,
			'comfort_fit' => $comfort_fit,
			'powertrain_fit' => $powertrain_fit,
			'handiness_fit' => $handiness_fit,
			'reliability_fit' => $reliability_fit,
			'multimedia_fit' => $multimedia_fit
		);

		return 1 / 8 * ($safety_fit + $fuel_cons_fit + $space_fit + $comfort_fit + $powertrain_fit + $handiness_fit + $reliability_fit + $multimedia_fit);
	}

	/**
	 * retrive all fit numbers from calculateAllCoefficients
	 * @param null
	 * @return array coefficients for all fit parameters or variables.
	 */
	public function returnArrayFit()
	{
		return $this->arrayFit;
	}

}