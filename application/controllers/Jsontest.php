<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jsontest extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		
	}

	
	public function index()
	{
		$this->load->view('templates/fortesting/jsontest.phtml', array( )); 


		// $arraytest = array(
		// 	'description' => 'test'
		// );

		// {description:'Choos your payment gateway', value:'', text:'Payment Gateway'},					
		// 			{image:'../images/msdropdown/icons/Amex-56.png', description:'My life. My card...', value:'amex', text:'Amex'},
		// 			{image:'../images/msdropdown/icons/Discover-56.png', description:'It pays to Discover...', value:'Discover', text:'Discover'},
		// 			{image:'../images/msdropdown/icons/Mastercard-56.png', title:'For everything else...', description:'For everything else...', value:'Mastercard', text:'Mastercard'},
		// 			{image:'../images/msdropdown/icons/Cash-56.png', description:'Sorry not available...', value:'cash', text:'Cash on devlivery', disabled:true},
		// 			{image:'../images/msdropdown/icons/Visa-56.png', description:'All you need...', value:'Visa', text:'Visa'},
		// 			{image:'../images/msdropdown/icons/Paypal-56.png', description:'Pay and get paid...', value:'Paypal', text:'Paypal'}

		// echo json_encode($arraytest);
	}

	public function arraytojson()
	{
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);
		// echo $format;
		$mainArray = array();

		if (strcasecmp($tags, "BMW")==0)
			$imageIcon = base_url().'assets-custom/images/msdropdown/icons/Amex-56.png';
		else
			$imageIcon = base_url().'assets-custom/images/msdropdown/icons/Mastercard-56.png';

		$array = array(
			'image' => $imageIcon,
			'description' => 'another test',
			'value' => 'amex',
			'text' => 'Amex'
		);

		for ($i=0; $i < 50; $i++) 
		{ 
			array_push($mainArray, $array);
		}

		echo json_encode($mainArray);
	}

	public function changeoptiontest()
	{
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);

		if (strcasecmp($tags, "BMW")==0)
			$this->load->view('templates/option/bodymodel.phtml', array('viewjs' => 'templates/advisoryprocess/dein-design-js')); 
		else
			$this->load->view('templates/option/bodymodel-test.phtml', array('viewjs' => 'templates/advisoryprocess/dein-design-js')); 
	}
		
}