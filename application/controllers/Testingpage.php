<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testingpage extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		
	}

	
	public function index()
	{
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Deine Design', // breadcrumbs, h2
			'what-nav' => 0,
			'what-step' => 2.75 // <nav>
		);

		$this->load->view(
        'testing.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/dein-design',
            'viewjs' => 'templates/advisoryprocess/dein-design-js'
        )); 


	}
}