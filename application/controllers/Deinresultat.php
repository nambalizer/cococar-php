<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deinresultat extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('result_model', 'resultmodel');
		$this->load->model('referenz_model', 'referenzmodel');
		$this->load->model('design_model', 'designmodel');
		$this->load->library('email');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
		

		if (!isset($this->advisorysession['uiPrice']) || (empty($this->advisorysession['uiPassenger']))) {
			redirect(base_url('deinegrundlagen'), 'refresh');
		}

		if (!empty($this->advisorysession['referenz']['carmaker']) &&
			!empty($this->advisorysession['referenz']['carmodel']) &&
			!empty($this->advisorysession['referenz']['carbodytype']) &&
			!empty($this->advisorysession['referenz']['cargeneration']) &&
			!empty($this->advisorysession['referenz']['carselected'])) {
			$this->arrayData = $this->advisorysession;
		} else {
			redirect(base_url('deinereferenz'), 'refresh');
		}

		if (!isset($this->advisorysession['wunschauto'])) {
			redirect(base_url('deinwunschauto'), 'refresh');	
		}
		
		if (!empty($this->advisorysession['deindesign']['cars'])) {
			redirect(base_url('deindesign'), 'refresh');
		}

		if (count($this->advisorysession['marken']['selectedlogos']) <= 0) {
			redirect(base_url('deinmarken'), 'refresh');
		}

		if ((count($this->advisorysession['deindesign']['selectedcars'])==0) ) {
			redirect(base_url('deinwunschauto'), 'refresh');
		}
	}
	
	public function index()
	{
		$error = false;
		$errorMessage = '';
		$arrayData = $this->arrayData;
		
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Dein Resultat', // breadcrumbs, h2
			'what-nav' => 0,
			'what-step' => 5,
			'active-page' => '', // <nav>,
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/advisoryprocess/template.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/dein-resultat',
            'viewjs' => 'templates/advisoryprocess/dein-resultat-js'
        )); 

        $this->lang->line('welcome_message');

	}

	public function mehrinformationen()
	{
		$arrayData = array();
		$error = false;
		$errorMessage = 'Please check for error';

		# get session
		$arrayData = $this->arrayData;
		$arraySelectedCar = array();
		$redirect = false;

        $idcar = filter_var($this->input->get('idcar'), FILTER_SANITIZE_STRING);
        

		$carMaker = $this->advisorysession['referenz']['carmaker'];
		$carModel = $this->advisorysession['referenz']['carmodel'];
		$carBodyType = $this->advisorysession['referenz']['carbodytype'];
		$carGeneration = $this->advisorysession['referenz']['cargeneration'];
		$carSelected = $this->advisorysession['referenz']['carselected'];
		
		$arrayData = $this->arrayData;
		$dataarray  = array();

		$dataarray =  $this->designmodel->getCar($this->advisorysession['wunschauto'], $idcar, $this->advisorysession['uiPrice'], $this->advisorysession['uiPassenger'], TRUE);
		$similar = $this->resultmodel->getSimilar($dataarray);
		// $bodytypes = $this->resultmodel->getSimilar($dataarray, $dataarray['carInformation']->engine);
		$bodytypes = $this->resultmodel->getSimilarBodyType($dataarray);
		// print_r($dataarray);

		// mehr-informationen.phtml
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Dein Resultat', // breadcrumbs, h2
			'data-results' => $dataarray,
			'similar' => $similar,
			'bodytypes' => $bodytypes,
			'what-nav' => 0,
			'what-step' => 5,
			'active-page' => ''// <nav>
		);

		$this->load->view(
        'templates/advisoryprocess/template-car-details.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/mehr-informationen',
            'viewjs' => 'templates/advisoryprocess/mehr-informationen-js'
        )); 
	}

	public function individualadvice()
	{
		$this->load->model('Customer_model','customer');
		$dataarray = array();
		$dataarray =  $this->arrayData;
		$display = array(
			'popup-title' => 'Persönliche Beratung', // <title>
			'popup-submitbutton' => 'Anfrage senden',
			'popup-action' => base_url('deinresultat/individualadvice'),
			'data-results' => $dataarray
		);

		if ($_POST)
		{
			$this->form_validation->set_rules('full_name','Full name', 'trim|required|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('phone','Phone', 'trim|required|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('messages','Messages', 'trim|required|min_length[3]|max_length[2500]');			

			if ($this->form_validation->run() == FALSE)
			{
				$error = true;
				$errorMessage = 'Please check for error';
				$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
			}
			else
			{
				$error = false;
				$errorMessage = '';
			}
			
			if ($error == false) {
				$dataarray['contact']['full_name'] = $formFullname = ucwords(filter_var($this->input->post('full_name'), FILTER_SANITIZE_STRING));
				$dataarray['contact']['email'] = $formEmail = filter_var($this->input->post('email'), FILTER_SANITIZE_STRING);
				$dataarray['contact']['phone'] = $formPhone = ucwords(filter_var($this->input->post('phone'), FILTER_SANITIZE_STRING));
				$dataarray['contact']['messages'] = $formMessages = ucwords(filter_var($this->input->post('messages'), FILTER_SANITIZE_STRING));

				$dataarray['customerinfo'] = $this->customer->individualadvice($dataarray['contact']);
				$dataarray['customerinfo']['coid'] = $this->customer->customer_option($dataarray, $dataarray['wunschauto'], $dataarray['referenz'], $dataarray['customerinfo']);
				$dataarray['customerinfo']['idcustomer_yourdesign'] = $this->customer->cardesigns($dataarray, $dataarray['customerinfo']);
				$this->customer->cardesigntime($dataarray);
				$dataarray['customerinfo']['idcar_makes'] = $this->customer->carmakes($dataarray, $dataarray['customerinfo']);

				$this->session->unset_userdata('advisorysession');
				$this->session->set_userdata('advisorysession', $dataarray);

				# in-house email for notification of order
				// $email = 'cococar.international@yandex.com';
				$email = 'cococar.international@gmail.com';

				$this->email->from('cococar.international@gmail.com', 'Cococar Information');
				$this->email->to($email);
				$this->email->subject('Individual advice');

				$this->email->message(
				$this->load->view(
				'templates/email/email.phtml', array('title' => 'Emailer', 
				'view' => 'email/forcococar', 'emailData' => $dataarray, 'footerEmail' => $email), true));

				# --------------[ NEED ATTENTION ]-------------
				# un-comment this when launching
				$this->email->send();
			} 

		} else {
			$this->load->view(
	        	'templates/advisoryprocess/template-popup.phtml', array(
	            'display' => $display,
	            'emailData' => $display,
	            'view' => 'templates/advisoryprocess/deinresultat-individualadvice',
	            'viewjs' => 'templates/advisoryprocess/deinresultat-individualadvice-js'
	        )); 	
		}		
	}

	public function carcomparison()
	{
		$dataarray =  $this->arrayData;
		$arrayData = array();
		$error = false;
		$errorMessage = 'Please check for error';

		$arrayData['allmakes'] = $this->referenzmodel->showCarMakes();
	

		if ($_POST) {
			$this->form_validation->set_rules('idcar','idcar', 'trim|required');

			if ($this->form_validation->run() == FALSE)
			{
				$error = true;
				$errorMessage = 'Please check for error';
				$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
			}
			else
			{
				$error = false;
				$errorMessage = '';
			}
			
			$idcar = ucwords(filter_var($this->input->post('idcar'), FILTER_SANITIZE_STRING));

			
			$arrayForSession =  $this->designmodel->getCar($this->advisorysession['wunschauto'], $idcar, $this->advisorysession['uiPrice'], $this->advisorysession['uiPassenger']);
			array_push($this->advisorysession['deindesign']['selectedcars'], $arrayForSession);
			$this->session->unset_userdata('advisorysession');
			$this->session->set_userdata('advisorysession', $this->advisorysession);
			redirect(base_url('deinresultat'), 'refresh');
		} else {

		}

		$display = array(
			'popup-title' => 'Auto zum Vergleich hinzufügen', // <title>
			'popup-submitbutton' => 'Auto hinzufügen',
			'popup-action' => base_url('deinresultat/carcomparison'),
			'data-results' => $dataarray,
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/advisoryprocess/template-popup.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/deinresultat-addcarcomparison'
        )); 
	}

	public function algotest()
	{
		$this->resultmodel->algotest();
	}

	public function modulotest()
	{
		echo (0%4);
	}
}