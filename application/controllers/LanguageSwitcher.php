<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Languageswitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();     
    }
 
    function switchLang($language = "german") {
        
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        
        redirect($_SERVER['HTTP_REFERER']);
        
        // $this->load->view(
        // 'templates/advisoryprocess/languageswitch-sample.phtml', array()); 
    }


}