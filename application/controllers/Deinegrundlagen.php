<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deinegrundlagen extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('grundlagen_model', 'grundlagenmodel');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
	}
	
	public function index()
	{
		$arrayData = array();
		$error = false;
		$errorMessage = 'Please check for error';

        if ($_POST) {
        	$this->form_validation->set_rules('uiPrice', 'money value of car', 'trim|required');
        	$this->form_validation->set_rules('uiPassenger', 'passengers', 'trim|required');

        	$arrayData['uiPrice'] = $moneynisiya = ucwords(filter_var($this->input->post('uiPrice'), FILTER_SANITIZE_STRING));
        	$arrayData['uiPassenger'] = $moneynisiya = ucwords(filter_var($this->input->post('uiPassenger'), FILTER_SANITIZE_STRING));

        	if ($this->form_validation->run() == FALSE) {
        		$error = true;
				$errorMessage = 'Please check for error';
				$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
			} else {
				// $error = FALSE;
				$this->session->set_userdata('advisorysession', $arrayData);
				redirect(base_url('deinereferenz'), 'refresh');
			}

        } else { // retrieve from the session instead.
        	$arrayData = $this->advisorysession;
        }

            $display = array(
				'page-title' => 'Advisory Process', // <title>
				'what-process' =>  'Deine Grundlagen', // breadcrumbs, h2
				'what-nav' => 0,
				'active-page' => '',
				'what-step' => 1.5, // <nav>,
				'arrayData' => $arrayData
			);

			$this->load->view(
	        'templates/advisoryprocess/template.phtml', array(
	            'display' => $display,
	            'view' => 'templates/advisoryprocess/deine-grundlagen',
	            'viewjs' => 'templates/advisoryprocess/deine-grundlagen-js'
	        )); 

	        // $this->lang->line('welcome_message');
	}

	public function backtostart()
	{
		$this->session->unset_userdata('advisorysession');
		$this->session->unset_userdata('errorsession');

		redirect(base_url('deinegrundlagen'), 'refresh');
	}
}