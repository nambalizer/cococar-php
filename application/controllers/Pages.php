<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
	}

	
	public function index()
	{
		redirect(base_url(), 'refresh');
	}

	public function termsandconditions()
	{
		$arrayBreadCrumbs = array(
			'home' => base_url(),
			'Geschäftsbedingungen' => base_url('termsandconditions')
		);
		$display = array(
			'page-title' => 'Geschäftsbedingungen', // <title>
			'what-process' =>  'Deine Grundlagen', // breadcrumbs, h2
			'what-nav' => 0,
			'breadcrumbs' => $arrayBreadCrumbs,
			'active-page' => ''
		);

		$this->load->view(
        'templates/insidepage.phtml', array(
            'display' => $display,
            'view' => 'pages/termsandconditions'
        )); 
	}

	public function privacy()
	{
		$arrayBreadCrumbs = array(
			'home' => base_url(),
			'Privatsphäre' => base_url('privacy')
		);
		$display = array(
			'page-title' => 'Privatsphäre', // <title>
			'what-process' =>  'Deine Grundlagen', // breadcrumbs, h2
			'what-nav' => 0,
			'breadcrumbs' => $arrayBreadCrumbs,
			'active-page' => ''
		);

		$this->load->view(
        'templates/insidepage.phtml', array(
            'display' => $display,
            'view' => 'pages/privacy'
        )); 
	}
}