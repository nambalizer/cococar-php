<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deindesign extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('design_model', 'designmodel');
		$this->load->model('wunschauto_model', 'wunschautomodel');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
		

		if (!isset($this->advisorysession['uiPrice']) || (empty($this->advisorysession['uiPassenger']))) {
			redirect(base_url('deinegrundlagen'), 'refresh');
		}

		if (!empty($this->advisorysession['referenz']['carmaker']) &&
			!empty($this->advisorysession['referenz']['carmodel']) &&
			!empty($this->advisorysession['referenz']['carbodytype']) &&
			!empty($this->advisorysession['referenz']['cargeneration']) &&
			!empty($this->advisorysession['referenz']['carselected'])) {
			$this->arrayData = $this->advisorysession;
		} else {
			redirect(base_url('deinereferenz'), 'refresh');
		}

		if (!isset($this->advisorysession['wunschauto'])) {
			redirect(base_url('deinwunschauto'), 'refresh');	
		}

		
	}

	
	public function index()
	{
		$page = 1;
		$arrayData = $this->arrayData;
		$arraySelectedCar = array();
		$newArray = array();
		$redirect = false;

        $booleanYes = filter_var($this->input->get('like'), FILTER_SANITIZE_STRING);

        # last car is yes
		if(!empty($booleanYes)) {
			if (strcasecmp($booleanYes, 'true')==0) {
				if (!empty($arrayData['deindesign']['carfornow']))
				array_push($arrayData['deindesign']['selectedcars'], $arrayData['deindesign']['carfornow']);
			}
		}

        # retain selected cars session in an array;
        if (!empty($arrayData['deindesign']['selectedcars'])) {
        	$arraySelectedCar = $this->arrayData['deindesign']['selectedcars'];
        } else {

        	$arrayData['deindesign']['selectedcars'] = array();
        	
        }

        # if session is empty put something on it
		if(empty($this->advisorysession['deindesign']['cars']) ) {
			if (!empty($booleanYes))
        		$redirect = true;
        	else
			$arrayData['deindesign']['cars'] = $this->designmodel->getCars($this->advisorysession['wunschauto'], $this->advisorysession['uiPrice'], $this->advisorysession['uiPassenger']);	
			
			# get 5 cars only
			if (count($arrayData['deindesign']['cars']) > 5)
			{
				$counter = 5;
				while($counter >= 1) {
					$fromArrray = array_pop($arrayData['deindesign']['cars']);	
					array_push($newArray, $fromArrray);
					$counter--;
				}
				
				if (isset($newArray)) {
					usort($newArray, $this->build_sorter('totalCoefficient'));
				}

				$arrayData['deindesign']['cars'] = $newArray;
			}
			# end of get 5 cars only

			if (count($arrayData['deindesign']['cars'])== 0) {

			}
		}

		#pull a car from the array
		$arrayData['deindesign']['carfornow'] = array_pop($arrayData['deindesign']['cars']);	
			

		$this->session->unset_userdata('advisorysession');
    	$this->session->set_userdata('advisorysession', $arrayData);
		if ($redirect == true)
			redirect(base_url('deinmarken'),'refresh');


		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Deine Design', // breadcrumbs, h2
			'what-nav' => 0,
			'active-page' => '',
			'what-step' => 2.75, // <nav>
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/advisoryprocess/template.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/dein-design',
            'viewjs' => 'templates/advisoryprocess/dein-design-js'
        )); 

        $this->lang->line('welcome_message');

	}

	private function build_sorter($key) {
	    return function ($a, $b) use ($key) {
	    	if($a[$key]==$b[$key]) return 0;
    			return $a[$key] > $b[$key]?1:-1;
	    	// return $a[$key] < $b[$key] 
	     //    return strnatcmp($a[$key], $b[$key]);
	    };
	}


	public function sessionjson()
	{
		header('Content-Type: application/json');
		echo json_encode($this->advisorysession['wunschauto']);
	}

	public function logtime()
	{
		$seconds = filter_var($this->input->get('milliseconds'), FILTER_SANITIZE_STRING);
		$car = filter_var($this->input->get('car'), FILTER_SANITIZE_STRING);
		$arrayData = $this->arrayData;

		if ((!empty($seconds)) && (!empty($car))) {

			if (empty($arrayData['deindesign']['timespent'])) {
				$arrayData['deindesign']['timespent'] = array();
			}
			
			$arrayTimespent =  array(
				'car' => $car,
				'milliseconds' => $seconds
			);

			array_push($arrayData['deindesign']['timespent'], $arrayTimespent);
			// print_r($arrayData['deindesign']['timespent']);


			$this->session->unset_userdata('advisorysession');
    		$this->session->set_userdata('advisorysession', $arrayData);
		}

	}
}