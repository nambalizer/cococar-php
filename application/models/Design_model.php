<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Design_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('cococaralgorithm');
		$this->highestScore = 3.5;
		$this->lowestScore = 2;
	}

	public function getCars($desiredRatings, $desiredPrice, $desiredPassengers)
	{
		$arrayReturn = array();
		$arrayForPush = array();
		$this->db->where('dprice <=', $desiredPrice);
		$this->db->where('seats >=', $desiredPassengers);
		$this->db->from('car');

		$result = $this->db->get()->result();

		foreach ($result as $value) {
			// $arrayDb = $this->wunschauto->getRatings($this->advisorysession['referenz']['carselected']->idcar);
			$arrayDb = array(
				'safety_db' => $value->safety,
	 			'fuel_cons_db' => $value->fuelcons,
	 			'space_db' => $value->space,
	 			'comfort_db' => $value->comfort,
	 			'powertrain_db' => $value->powertrain,
	 			'handniness_db' => $value->handiness,
	 			'reliability_db' => $value->reliability,
	 			'multimedia_db' => $value->multimedia
			);
			$this->cococaralgorithm->populateFromDb($arrayDb);

			$arrayDesired = array(
				'safety_desired' => $desiredRatings['newSicherheit'],
				'fuel_cons_desired' => $desiredRatings['newVerbrauch'],
				'space_desired' => $desiredRatings['newPlatzangebot'],
				'comfort_desired' => $desiredRatings['newKomfort'],
				'powertrain_desired' => $desiredRatings['newAntrieba'],
				'handiness_desired' => $desiredRatings['newHandlichkeit'],
				'reliability_desired' => $desiredRatings['newZuverlassigkeit'],
				'multimedia_desired' => $desiredRatings['newMultimedia']
			);
			$arrayForPush['totalCoefficient'] = $this->cococaralgorithm->calculateAllCoefficients($arrayDesired);
			$arrayForPush['coefficients'] = $this->cococaralgorithm->returnArrayFit();
			$arrayForPush['carInformation'] = $value;

			# get the pictures
			$this->db->from('car_has_car_picture as has');
			$this->db->where('has.car_idcar', $value->idcar);
			$this->db->where('picture.fordesign', TRUE);
			$this->db->join('car_picture AS picture', 'has.car_picture_idcar_picture = picture.idcar_picture');
			$arrayForPush['carpicture'] = $this->db->get()->row();

			# get the higher and lower value of the car, PROS
			$arrayForPush['pros'] = array();
			if ($value->safety > $this->highestScore)
				array_push($arrayForPush['pros'], 'Sicherheit');
			if ($value->fuelcons > $this->highestScore)
				array_push($arrayForPush['pros'], 'Verbrauch');
			if ($value->space > $this->highestScore)
				array_push($arrayForPush['pros'], 'Platzengebot');
			if ($value->comfort > $this->highestScore)
				array_push($arrayForPush['pros'], 'Komfort');
			if ($value->environment > $this->highestScore)
				array_push($arrayForPush['pros'], 'Umwelt');
			if ($value->powertrain > $this->highestScore)
				array_push($arrayForPush['pros'], 'Antrieb');
			if ($value->driving > $this->highestScore)
				array_push($arrayForPush['pros'], 'Fahrspaß');
			if ($value->handiness > $this->highestScore)
				array_push($arrayForPush['pros'], 'Handlichkeit');
			if ($value->reliability > $this->highestScore)
				array_push($arrayForPush['pros'], 'Zuverlässigkeit');
			if ($value->multimedia > $this->highestScore)
				array_push($arrayForPush['pros'], 'Multimedia');

			# get the higher and lower value of the car, CONS
			$arrayForPush['cons'] = array();
			if ($value->safety < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Sicherheit');
			if ($value->fuelcons < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Verbrauch');
			if ($value->space < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Platzengebot');
			if ($value->comfort < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Komfort');
			if ($value->environment < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Umwelt');
			if ($value->powertrain < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Antrieb');
			if ($value->driving < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Fahrspaß');
			if ($value->handiness < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Handlichkeit');
			if ($value->reliability < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Zuverlässigkeit');
			if ($value->multimedia < $this->lowestScore)
				array_push($arrayForPush['cons'], 'Multimedia');

			array_push($arrayReturn, $arrayForPush);
		}

		if (isset($arrayReturn)) {
			usort($arrayReturn, $this->build_sorter('totalCoefficient'));

			return $arrayReturn;	
		} else {
			return ;
		}
	}

	public function getCar($desiredRatings, $idcar, $desiredPrice, $desiredPassengers, $manypictures = FALSE)
	{
		$arrayReturn = array();
		$arrayForPush = array();
		$this->db->where('idcar', $idcar);
		$this->db->from('car');

		$result = $this->db->get()->row();

			// $arrayDb = $this->wunschauto->getRatings($this->advisorysession['referenz']['carselected']->idcar);
		$arrayDb = array(
			'safety_db' => $result->safety,
 			'fuel_cons_db' => $result->fuelcons,
 			'space_db' => $result->space,
 			'comfort_db' => $result->comfort,
 			'powertrain_db' => $result->powertrain,
 			'handniness_db' => $result->handiness,
 			'reliability_db' => $result->reliability,
 			'multimedia_db' => $result->multimedia
		);
		$this->cococaralgorithm->populateFromDb($arrayDb);

		$arrayDesired = array(
			'safety_desired' => $desiredRatings['newSicherheit'],
			'fuel_cons_desired' => $desiredRatings['newVerbrauch'],
			'space_desired' => $desiredRatings['newPlatzangebot'],
			'comfort_desired' => $desiredRatings['newKomfort'],
			'powertrain_desired' => $desiredRatings['newAntrieba'],
			'handiness_desired' => $desiredRatings['newHandlichkeit'],
			'reliability_desired' => $desiredRatings['newZuverlassigkeit'],
			'multimedia_desired' => $desiredRatings['newMultimedia']
		);
		if (($desiredPrice >= $result->dprice) && ($desiredPassengers <= $result->seats)) {
			$arrayForPush['totalCoefficient'] = $this->cococaralgorithm->calculateAllCoefficients($arrayDesired);
		} else {
			$arrayForPush['totalCoefficient'] = 0;
		}
		$arrayForPush['coefficients'] = $this->cococaralgorithm->returnArrayFit();
		$arrayForPush['carInformation'] = $result;

		# get the pictures
		$this->db->from('car_has_car_picture as has');
		$this->db->where('has.car_idcar', $result->idcar);
		$this->db->where('picture.fordesign', TRUE);
		$this->db->join('car_picture AS picture', 'has.car_picture_idcar_picture = picture.idcar_picture');
		if($manypictures == FALSE) {
			$arrayForPush['carpicture'] = $this->db->get()->row();
		} else {
			$arrayForPush['carpicture'] = $this->db->get()->result();
		}

		# get the higher and lower value of the car, PROS
		$arrayForPush['pros'] = array();
		if ($result->safety > $this->highestScore)
			array_push($arrayForPush['pros'], 'Sicherheit');
		if ($result->fuelcons > $this->highestScore)
			array_push($arrayForPush['pros'], 'Verbrauch');
		if ($result->space > $this->highestScore)
			array_push($arrayForPush['pros'], 'Platzengebot');
		if ($result->comfort > $this->highestScore)
			array_push($arrayForPush['pros'], 'Komfort');
		if ($result->environment > $this->highestScore)
			array_push($arrayForPush['pros'], 'Umwelt');
		if ($result->powertrain > $this->highestScore)
			array_push($arrayForPush['pros'], 'Antrieb');
		if ($result->driving > $this->highestScore)
			array_push($arrayForPush['pros'], 'Fahrspaß');
		if ($result->handiness > $this->highestScore)
			array_push($arrayForPush['pros'], 'Handlichkeit');
		if ($result->reliability > $this->highestScore)
			array_push($arrayForPush['pros'], 'Zuverlässigkeit');
		if ($result->multimedia > $this->highestScore)
			array_push($arrayForPush['pros'], 'Multimedia');

		# get the higher and lower value of the car, CONS
		$arrayForPush['cons'] = array();
		if ($result->safety < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Sicherheit');
		if ($result->fuelcons < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Verbrauch');
		if ($result->space < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Platzengebot');
		if ($result->comfort < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Komfort');
		if ($result->environment < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Umwelt');
		if ($result->powertrain < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Antrieb');
		if ($result->driving < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Fahrspaß');
		if ($result->handiness < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Handlichkeit');
		if ($result->reliability < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Zuverlässigkeit');
		if ($result->multimedia < $this->lowestScore)
			array_push($arrayForPush['cons'], 'Multimedia');

		
		return $arrayForPush;
	}

	public function justhereference($desiredRatings) {
		
	}

	private function build_sorter($key) {
	    return function ($a, $b) use ($key) {
	    	if($a[$key]==$b[$key]) return 0;
    			return $a[$key] > $b[$key]?1:-1;
	    	// return $a[$key] < $b[$key] 
	     //    return strnatcmp($a[$key], $b[$key]);
	    };
	}
}