$(document).ready(function(e) 
{
	$('.icon-resultat').animate({opacity: 0.2}, 1000);
	$("#icon-trailer").click(function(){
		if($(this).css('opacity') == 1){
			$(this).animate({opacity:0.2}, 1000);
			$('#checkbox-trailer').val(0);
		} else{
			$(this).animate({opacity:1}, 1000);
			$('#checkbox-trailer').val(1);
		}
	});
	$("#icon-electriccar").click(function(){
		if($(this).css('opacity') == 1){
			$(this).animate({opacity:0.2}, 1000);
			$('#checkbox-electriccar').val(0);
		} else{
			$(this).animate({opacity:1}, 1000);
			$('#checkbox-electriccar').val(1);
		}
	});
	$("#icon-seatbelt").click(function(){
		if($(this).css('opacity') == 1){
			$(this).animate({opacity:0.2}, 1000);
			$('#checkbox-seatbelt').val(0);
		} else{
			$(this).animate({opacity:1}, 1000);
			$('#checkbox-seatbelt').val(1);
		}
	});
	$("#icon-automatic").click(function(){
		if($(this).css('opacity') == 1){
			$(this).animate({opacity:0.2}, 1000);
			$('#checkbox-automatic').val(0);
		} else{
			$(this).animate({opacity:1}, 1000);
			$('#checkbox-automatic').val(1);
		}
	});
	$("#icon-chasis").click(function(){
		if($(this).css('opacity') == 1){
			$(this).animate({opacity:0.2}, 1000);
			$('#checkbox-chasis').val(0);
		} else{
			$(this).animate({opacity:1}, 1000);
			$('#checkbox-chasis').val(1);
		}
	});

	$("#individual-advice").click(function(){
		$('#popup-content').html('<img src="assets/images/loaders/7.gif" alt="" />');
		$('#popup-content').load(BASEURLL+'deinresultat/individualadvice');
	});

	$("#car-comparison").click(function(){
		$('#popup-content').html('<img src="assets/images/loaders/7.gif" alt="" />');
		$('#popup-content').load(BASEURLL+'deinresultat/carcomparison');
	});

	$('.button-sharer').click(function(event) {
	    event.preventDefault();
	    window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");
	});

});