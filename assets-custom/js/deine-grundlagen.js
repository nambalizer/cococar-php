jQuery(window).ready(function() {
	
	loadScript(plugin_path + 'jquery/jquery-ui.min.js', function() { /** jQuery UI **/
		loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function() { /** Mobile Touch Slider **/
			loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function() { /** Slider Script **/

				/** Slider for price
				******************** **/
				jQuery("#slider5").slider({
					value: defaultMoneyvalue,
					animate: true,
					min: 0, //10000,
					max: maxvalueslider,
					step: 500,
					range: "min",
					slide: function(event, ui) {
						// if (jQuery("#moneyvalue").val() < ui.value) {						
						// 	// $('#moneynisiya').prepend('<i class="fa fa-money moneyfigure" aria-hidden="true" id="moneynisiya'+jQuery("#moneyvalue").val()+'" ></i>');
						// }
						// else if (jQuery("#moneyvalue").val() > ui.value) {
						// 	// $('#moneynisiya'+jQuery("#moneyvalue").val()).remove();
						// }
						// else{}
						jQuery("#moneyvalue").val(ui.value);

						// for the label
						var thumb = $($('#slider5').children('.ui-slider-handle'));
						
						$('#price-div').css('left', thumb.position().left - ($('#price-div').width() - thumb.width())/ 2); 
						$('#price-div').css('top', thumb.position().top-35);

						var percentagevalue = Math.round((jQuery("#moneyvalue").val() / maxvalueslider) * 40);
						$('#price-div').html(jQuery("#moneyvalue").val()+'€');
						
						jQuery("#moneydisplay").empty();
						$('#moneydisplay').html('<i class="fa fa-money moneyfigure" aria-hidden="true" id="moneynisiya" ></i>');
						for (counterni = 1; counterni < percentagevalue; counterni++) { 
						    $('#moneynisiya').after('<i class="fa fa-money moneyfigure" aria-hidden="true" ></i>');
						    if ((counterni%8) == 0)
						    	$('#moneynisiya').after('<br>');
						} 
					}
				});
				
				jQuery("#moneyvalue").val(jQuery("#slider5").slider("value"));
				jQuery("#moneyvalue").blur(function() {
						jQuery("#slider5").slider("value", jQuery(this).val());
				});
				jQuery("#slider5").slider("float", { prefix: "", suffix: "€", pips: true });


				/** slider for passengers
				******************** **/
				jQuery("#slider-passengers").slider({
					value: defaultPassengersvalue,
					animate: true,
					min: 1,
					max: maxvaluesliderpassenger,
					step: 1,
					range: "min",
					slide: function(event, ui) {
						jQuery("#text-passengers").val(ui.value);
						console.log(ui);

						var percentagevaluepassenger = Math.round((jQuery("#text-passengers").val() / maxvaluesliderpassenger) * 8);
						$('#passenger-div').html(jQuery("#text-passengers").val());

						jQuery("#passengerdisplay").empty();
						$('#passengerdisplay').html('<img src="'+BASEURLL+'assets-custom/images/icon-passenger.png" alt="" id="passengersiya" / >');
						for (counterni = 1; counterni < percentagevaluepassenger; counterni++) { 
						    $('#passengersiya').after('<img src="'+BASEURLL+'assets-custom/images/icon-passenger.png" alt="" / >');
						} 

						movepassenger();
					}
				});

				function movepassenger(){
					var thumbpass = $($('#slider-passengers').children('.ui-slider-handle'));
						// $('.ui-slider-tip').hide();
						$('#passenger-div').css('left', thumbpass.position().left - ($('#passenger-div').width() - thumbpass.width())/ 2); 
				}

				// slide passenger tool tip fix
				$('#slider-passengers').mouseup(function() {
				  movepassenger();
				});
				$('#slider-passengers').mouseout(function() {
				  movepassenger();
				});


				 // var thumbilina = $($('#slider-passengers').children('.ui-slider-handle'));   
			  //   setLabelPosition();    
			    
			  //   $('#slider-passengers').bind('slide', function () {        
			  //       $('#passenger-div').val($('#slider-passengers').slider('value'));
			  //       setLabelPosition();
			  //   });
			    
			  //   function setLabelPosition() {
			  //       var label = $('#passenger-div');
			  //       label.css('top', thumbilina.position().top + label.outerHeight(true)+35);
			  //       label.css('left', thumbilina.position().left - (label.width() - thumbilina.width())/ 2);        
			  //   }

				
				jQuery("#text-passengers").val(jQuery("#slider-passengers").slider("value"));
				jQuery("#text-passengers").blur(function() {
						jQuery("#slider-passengers").slider("value", jQuery(this).val());
				});
				jQuery("#slider-passengers").slider("float", { prefix: "", suffix: "", pips: true });
				var thumbPrice = $($('#slider5').children('.ui-slider-handle'));
				var thumbPassenger = $($('#slider-passengers').children('.ui-slider-handle'));
			  	$('#price-div').css('top', $('#price-div').position().top-39);
			  	$('#price-div').css('left', thumbPrice.position().left - ($('#price-div').width() - thumbPrice.width())/ 2); 
			  	$('#passenger-div').css('top', $('#passenger-div').position().top-39);
			  	$('#passenger-div').css('left', thumbPassenger.position().left - ($('#passenger-div').width() - thumbPassenger.width())/ 2); 
			  	$('.ui-slider-tip').hide();
			});
		});
	});

});