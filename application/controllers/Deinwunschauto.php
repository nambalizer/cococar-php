<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deinwunschauto extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('wunschauto_model', 'wunschautomodel');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');

		if (!isset($this->advisorysession['uiPrice']) || (empty($this->advisorysession['uiPassenger']))) {
			redirect(base_url('deinegrundlagen'), 'refresh');
		}
		if (!empty($this->advisorysession['referenz']['carmaker']) &&
			!empty($this->advisorysession['referenz']['carmodel']) &&
			!empty($this->advisorysession['referenz']['carbodytype']) &&
			!empty($this->advisorysession['referenz']['cargeneration']) &&
			!empty($this->advisorysession['referenz']['carselected'])) {
			$this->arrayData = $this->advisorysession;
		} else {
			redirect(base_url('deinereferenz'), 'refresh');
		}
	}

	
	public function index()
	{
		$arrayData = array();
		$error = false;
		$errorMessage = 'Please check for error';

		$carMaker = $this->advisorysession['referenz']['carmaker'];
		$carModel = $this->advisorysession['referenz']['carmodel'];
		$carBodyType = $this->advisorysession['referenz']['carbodytype'];
		$carGeneration = $this->advisorysession['referenz']['cargeneration'];
		$carSelected = $this->advisorysession['referenz']['carselected'];
		
		$arrayData = $this->arrayData;

		$arrayData['wunschauto']['car-ratings'] = $this->wunschautomodel->getRatings($carSelected->idcar, true);
		
		if ($_POST) {
			$this->form_validation->set_rules('points-sicherheit', 'sicherheit', 'trim|required');
        	$this->form_validation->set_rules('points-verbrauch', 'verbrauch', 'trim|required');
        	$this->form_validation->set_rules('points-platzangebot', 'platzangebot', 'trim|required');
        	$this->form_validation->set_rules('points-komfort', 'komfort', 'trim|required');
        	$this->form_validation->set_rules('points-antrieb', 'antrieb', 'trim|required');
        	$this->form_validation->set_rules('points-handlichkeit', 'handlichkeit', 'trim|required');
        	$this->form_validation->set_rules('points-zuverlassigkeit', 'zuverlassigkeit', 'trim|required');
        	$this->form_validation->set_rules('points-multimedia', 'multimedia', 'trim|required');
        	

        	$arrayData['wunschauto'] = array(
				'newSicherheit' => ucwords(filter_var($this->input->post('points-sicherheit'), FILTER_SANITIZE_STRING)),
        		'newVerbrauch' => ucwords(filter_var($this->input->post('points-verbrauch'), FILTER_SANITIZE_STRING)),
        		'newPlatzangebot' => ucwords(filter_var($this->input->post('points-platzangebot'), FILTER_SANITIZE_STRING)),
        		'newKomfort' => ucwords(filter_var($this->input->post('points-komfort'), FILTER_SANITIZE_STRING)),
        		'newAntrieba' => ucwords(filter_var($this->input->post('points-antrieb'), FILTER_SANITIZE_STRING)),
        		'newHandlichkeit' =>  ucwords(filter_var($this->input->post('points-handlichkeit'), FILTER_SANITIZE_STRING)),
        		'newZuverlassigkeit' => ucwords(filter_var($this->input->post('points-zuverlassigkeit'), FILTER_SANITIZE_STRING)),
        		'newMultimedia' => ucwords(filter_var($this->input->post('points-multimedia'), FILTER_SANITIZE_STRING)),
        	);

        	if ($error == true) {
        		$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
        	} else {
        		$this->session->unset_userdata('advisorysession');
        		$this->session->set_userdata('advisorysession', $arrayData);
				redirect(base_url('deindesign'), 'refresh');
        	}
		} else {
			
		}


		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Deine Wunschauto', // breadcrumbs, h2
			'what-nav' => 0,
			'what-step' => 2.5,
			'active-page' => '', // <nav>
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/advisoryprocess/template.phtml', array(
            'display' => $display,
            'view' => 'templates/advisoryprocess/dein-wunschauto',
            'viewjs' => 'templates/advisoryprocess/dein-wunschauto-js'
        )); 

        $this->lang->line('welcome_message');

	}
}