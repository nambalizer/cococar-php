<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testdrive extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Customer_model','customer');
		$this->load->model('design_model', 'designmodel');
		$this->load->library('email');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
	}

	
	public function index()
	{
        $idcar = filter_var($this->input->get('idcar'), FILTER_SANITIZE_STRING);
		$dataarray = $this->advisorysession;
        $dataarray['carfortestdrive'] =  $this->designmodel->getCar($this->advisorysession['wunschauto'], $idcar, $this->advisorysession['uiPrice'], $this->advisorysession['uiPassenger'], TRUE);

		$display = array(
			'popup-title' => 'Persönliche Beratung', // <title>
			'popup-submitbutton' => 'Anfrage senden',
			'popup-action' => base_url('testdrive?idcar='.$idcar),
			'data-results' => $dataarray
		);

		if ($_POST)
		{
			$this->form_validation->set_rules('full_name','Full name', 'trim|required|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('appointmentdate','Appointment Date', 'trim|required|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('email','Email', 'trim|required|valid_email|min_length[3]|max_length[250]');
			$this->form_validation->set_rules('phone','Phone', 'trim|max_length[250]');
			$this->form_validation->set_rules('messages','Messages', 'trim|required|min_length[3]|max_length[2500]');			

			if ($this->form_validation->run() == FALSE)
			{
				$error = true;
				$errorMessage = 'Please check for error';
				$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
			}
			else
			{
				$error = false;
				$errorMessage = '';
			}

			if ($error == false) {

				$dataarray['testdriveappointment']['full_name'] = ucwords(filter_var($this->input->post('full_name'), FILTER_SANITIZE_STRING));
				$dataarray['testdriveappointment']['appointmentdate'] = ucwords(filter_var($this->input->post('appointmentdate'), FILTER_SANITIZE_STRING));
				$dataarray['testdriveappointment']['email'] = ucwords(filter_var($this->input->post('email'), FILTER_SANITIZE_STRING));
				$dataarray['testdriveappointment']['phone'] =  ucwords(filter_var($this->input->post('phone'), FILTER_SANITIZE_STRING));
				$dataarray['testdriveappointment']['messages'] =  ucwords(filter_var($this->input->post('messages'), FILTER_SANITIZE_STRING));
				$dataarray['testdriveappointment']['idcar'] = $idcar;

				// $dataarray['customerinfo'] = $this->customer->individualadvice($dataarray['testdriveappointment']);
				$dataarray['customerinfo'] = $this->customer->setappointment($dataarray['testdriveappointment'], $display['data-results']['carfortestdrive']['totalCoefficient']);
				$dataarray['customerinfo']['coid'] = $this->customer->customer_option($dataarray, $dataarray['wunschauto'], $dataarray['referenz'], $dataarray['customerinfo']);
				$dataarray['customerinfo']['idcustomer_yourdesign'] = $this->customer->cardesigns($dataarray, $dataarray['customerinfo']);
				$dataarray['customerinfo']['idcar_makes'] = $this->customer->carmakes($dataarray, $dataarray['customerinfo']);

				# in-house email for notification of order
				// $email = 'cococar.international@yandex.com';
				$email = 'cococar.international@gmail.com';

				$this->email->from('cococar.international@gmail.com', 'Cococar Information');
				$this->email->to($email);
				$this->email->subject('Test drive request');

				$this->email->message(
				$this->load->view(
				'templates/email/email.phtml', array('title' => 'Emailer', 
				'view' => 'email/testdrive', 'emailData' => $dataarray, 'footerEmail' => $email), true));

				# --------------[ NEED ATTENTION ]-------------
				# un-comment this when launching
				$this->email->send();
			}

		} else {
			$this->load->view(
	        	'templates/advisoryprocess/template-popup.phtml', array(
	            'display' => $display,
	            'view' => 'templates/testdrive/index'
	        )); 	
		}
	}

	public function all()
	{
		$arrayData = array();
		$arrayData = $this->customer->retrievealltestdrive();

		// print_r($arrayData);
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-nav' => 0,
			'active-page' => '',
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/testdrive/template.phtml', array(
            'display' => $display,
            'view' => 'templates/testdrive/all'
        )); 
	}

	public function moreinformation()
	{
		$arrayData = array();
		$id = filter_var($this->input->get('id'), FILTER_SANITIZE_STRING);
		$arrayData = $this->customer->retrievetestdrive($id);
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-nav' => 0,
			'active-page' => '',
			'arrayData' => $arrayData
		);

		$this->load->view(
        'templates/testdrive/template.phtml', array(
            'display' => $display,
            'view' => 'templates/testdrive/moreinformation'
        )); 
	}
}