<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Result_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('cococaralgorithm');
	}

	public function algotest()
	{
		$arrayDb = array(
			'safety_db' => 3,
 			'fuel_cons_db' => 3,
 			'space_db' => 3,
 			'comfort_db' => 3,
 			'powertrain_db' => 3,
 			'handniness_db' => 3,
 			'reliability_db' => 3,
 			'multimedia_db' => 3
		);
		$this->cococaralgorithm->populateFromDb($arrayDb);

		$arrayDesired = array(
			'safety_desired' => 3,
			'fuel_cons_desired' => 3,
			'space_desired' => 3,
			'comfort_desired' => 3,
			'powertrain_desired' => 3,
			'handiness_desired' => 3,
			'reliability_desired' => 3,
			'multimedia_desired' => 3
		);
		
		echo 100 * $this->cococaralgorithm->calculateAllCoefficients($arrayDesired).' %';

		echo '<br>all fit values:';
		print_r($this->cococaralgorithm->returnArrayFit());
	}

	public function getSimilar($dataArray, $engine = NULL)
	{
		$this->db->from('car');
		$this->db->where('make', $dataArray['carInformation']->make);
		$this->db->where('model', $dataArray['carInformation']->model);
		$this->db->order_by('fuel', 'ASC');
		$this->db->order_by('dprice', 'ASC');
		$this->db->where('car.isactive', TRUE);

		if (!empty($engine)) {
			$this->db->where('engine', $engine);
			$this->db->group_by('body');
			$this->db->order_by('body', 'ASC');
			$this->db->select('idcar, body, pictureurl,iconurl');
			$this->db->join('car_has_car_picture AS has','has.car_idcar = car.idcar');	
			$this->db->join('car_picture AS picture','has.car_picture_idcar_picture = picture.idcar_picture');
		} 
		$this->db->group_by('engine');

		$result = $this->db->get()->result();

		return $result;
	}

	public function getSimilarBodyType($dataArray)
	{
		$this->db->from('car');
		$this->db->where('make', $dataArray['carInformation']->make);
		$this->db->where('model', $dataArray['carInformation']->model);
		$this->db->order_by('fuel', 'ASC');
		$this->db->order_by('dprice', 'ASC');
		$this->db->where('car.isactive', TRUE);		
		
		$this->db->group_by('body');
		$this->db->order_by('body', 'ASC');
		$this->db->select('idcar, body, pictureurl,iconurl');
		$this->db->join('car_has_car_picture AS has','has.car_idcar = car.idcar');	
		$this->db->join('car_picture AS picture','has.car_picture_idcar_picture = picture.idcar_picture');

		$result = $this->db->get()->result();

		return $result;
	}

	

	public function getCar($idcar)
	{

	}

	
}