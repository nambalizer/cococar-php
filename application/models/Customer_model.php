<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('cococaralgorithm');
		$this->customerOption = array();
	}

	public function customer($dataArray = array())
	{
		$return = FALSE;
		$dataInsert = array(
			'dateofaction' => $this->currentDate,
			'fullname' => $dataArray['full_name'],
			'email' => $dataArray['email'],
			'phone' => $dataArray['phone'],
			'message' => $dataArray['messages']
		);
		if (!empty($dataArray['full_name'])) {

			$this->db->insert('customer_information', $dataInsert);
			$return = $this->db->insert_id();
		}

		return $return;
	}

	public function individualadvice($dataArray = array())
	{
		$return = FALSE;
		$idcustomer_information	= $this->customer($dataArray);
		$dataInsert = array(
			'customer_information_idcustomer_information' => $idcustomer_information,
			'current' => TRUE,
			'dateset' => $this->currentDate
		);

		if (!empty($dataArray['full_name'])) {
			$this->db->insert('customer_advice', $dataInsert);

			$idcustomer_advice = $this->db->insert_id();
			$return = array(
				'idcustomer_information' => $idcustomer_information,
				'idcustomer_advice' => $idcustomer_advice
			);
		}

		return $return;
	}

	public function setappointment($dataArray = array(), $totalCoefficient)
	{
		$return = FALSE;
		$idcustomer_information	= $this->customer($dataArray);
		$dataInsert = array(
			'customer_information_idcustomer_information' => $idcustomer_information,
			'current' => TRUE,
			'appointmentdate' => $dataArray['appointmentdate'],
			'dateset' => $this->currentDate
		);
		
		if ( (!empty($dataArray['full_name'])) && (!empty($idcustomer_information)) ) {
			$this->db->insert('customer_appointment', $dataInsert);

			$idcustomer_appointment = $this->db->insert_id();
			$dataInsert = array(
				'customer_appointment_idcustomer_appointment' => $idcustomer_appointment,
				'car_idcar' => $dataArray['idcar'],
				'isactive' => TRUE,
				'percentage' => round($totalCoefficient, 4),
				'isactive' => TRUE
			);

			$this->db->insert('customer_appointment_has_car', $dataInsert);
			$customer_appointment_has_carid = $this->db->insert_id();

			$return = array(
				'idcustomer_information' => $idcustomer_information,
				'idcustomer_appointment' => $idcustomer_appointment,
				'customer_appointment_has_carid' => $customer_appointment_has_carid
			);
		}

		return $return;
	} 

	public function customer_option($dataArray, $dataArrayDream = array(), $dataArrayReference = array(), $dataReturn = array())
	{
		$return = FALSE;
		$dataInsert = array(
			'price' => $dataArray['uiPrice'],
			'passengers' => $dataArray['uiPassenger'],
			'make' => $dataArrayReference['carmaker'],
			'model' => $dataArrayReference['carmodel'],
			'bodytype' => $dataArrayReference['carbodytype'],
			'generation' => $dataArrayReference['cargeneration'],
			'safety' => $dataArrayDream['newSicherheit'],
			'fuelconsumption' => $dataArrayDream['newVerbrauch'],
			'space' => $dataArrayDream['newPlatzangebot'],
			'comfort' => $dataArrayDream['newKomfort'],
			'powertrain' => $dataArrayDream['newAntrieba'],
			'handiness' => $dataArrayDream['newHandlichkeit'],
			'reliability' => $dataArrayDream['newZuverlassigkeit'],
			'multimedia' => $dataArrayDream['newMultimedia'],
			'dateofaction' => $this->currentDate,
			'current' => TRUE,
			'customer_information_idcustomer_information' => $dataReturn['idcustomer_information']
		);
		if (!empty($dataReturn['idcustomer_information'])) {
			$this->db->insert('customer_options', $dataInsert);	
			$return = $this->db->insert_id();
			$this->customerOption = $dataArrayDream;
		}
		
		return $return;
	}

	public function cardesigns($dataArray = array(), $dataCustomerInfo = array())
	{
		$return = array();
		foreach ($dataArray['deindesign']['selectedcars'] as $value) {
			$dataInsert = array(
				'car_idcar' => $value['carInformation']->idcar,
				'percentage' => $value['totalCoefficient'],
				'like' => TRUE,
				'customer_information_idcustomer_information' => $dataCustomerInfo['idcustomer_information']
			);

			if (!empty($dataCustomerInfo['idcustomer_information'])) {
				$this->db->insert('customer_yourdesign', $dataInsert);
				array_push($return, $this->db->insert_id());
			}
		}
		
		return $return;
	}

	public function cardesigntime($dataarray) 
	{
		foreach ($variable as $key => $value) {
			# code...
		}
		return;
	}

	public function carmakes($dataArray = array(), $dataCustomerInfo = array())
	{
		$return = array();
		foreach ($dataArray['marken']['selectedlogos'] as $value) {
			$dataInsert = array(
				'customer_information_idcustomer_information' => $dataCustomerInfo['idcustomer_information'],
				'car_makes_idcar_makes' => $value,
				'isactive' => TRUE
			);

			if (!empty($dataCustomerInfo['idcustomer_information'])) {
				$this->db->insert('customer_makes', $dataInsert);
				array_push($return, $this->db->insert_id());
			}
		}

		return $return;
	}

	public function retrieveAllNeedAdvice()
	{
		$return = array();
		// $this->db->select('allergy.IDsafe_allergen, list.allergenName, dateexperienced, severity, notes');
		// $this->db->from('customer_information as customer');
		// $this->db->join('allergenlist AS list','list.IDallergenList = allergy.IDallergenList');
		// $this->db->where('allergy.active', TRUE);
		// $this->db->where('allergy.IDsafe_personalInfo', $IDsafe_personalInfo);
		
		$this->db->order_by("idcustomer_information", "desc");
		$this->db->from('customer_information');
		$return = $this->db->get()->result();

		return $return;
	}

	public function retrieveNeedAdvice($idcustomer_information = NULL )
	{
		
		$return = array();
		$arrayForReturnForPush = array();
		if (!empty($idcustomer_information)) {
			$this->db->where('idcustomer_information', $idcustomer_information);
			$this->db->from('customer_information');
			$return['customer'] = $this->db->get()->row();
		}

		// get the customer desired option
		if (!empty($idcustomer_information)) {
			$this->db->where('customer_information_idcustomer_information', $idcustomer_information);
			$this->db->join('car_makes as makes','makes.idcar_makes = option.make');
			$this->db->from('customer_options AS option');
			$return['options'] = $this->db->get()->row();
		}

		// get the makes tables, if naa
		if (!empty($idcustomer_information)) {
			$this->db->join('car_makes AS makes', 'makes.idcar_makes = customer.car_makes_idcar_makes');
			$this->db->where('customer_information_idcustomer_information', $idcustomer_information);
			$this->db->from('customer_makes as customer');
			$returnMakes = $this->db->get()->result();
			if (count($returnMakes) > 0) {
				$return['makes'] = $returnMakes;
			}
		}

		// get the yourdesigns tables, if naa
		if (!empty($idcustomer_information)) {
			$this->db->join('car', 'car.idcar = design.car_idcar');
			$this->db->where('customer_information_idcustomer_information', $idcustomer_information);
			$this->db->from('customer_yourdesign as design');
			$returnDesign = $this->db->get()->result();
			if (count($returnDesign) > 0) {
				foreach ($returnDesign as $value) {
					$arrayForReturn = $value;

					# ---- compute compatiblity
					$arrayDb = array(
						'safety_db' => $value->safety,
			 			'fuel_cons_db' => $value->fuelcons,
			 			'space_db' => $value->space,
			 			'comfort_db' => $value->comfort,
			 			'powertrain_db' => $value->powertrain,
			 			'handniness_db' => $value->handiness,
			 			'reliability_db' => $value->reliability,
			 			'multimedia_db' => $value->multimedia
					);
					$this->cococaralgorithm->populateFromDb($arrayDb);
					$arrayDesired = array(
						'safety_desired' => $return['options']->safety,
						'fuel_cons_desired' => $return['options']->fuelconsumption,
						'space_desired' => $return['options']->space,
						'comfort_desired' => $return['options']->comfort,
						'powertrain_desired' => $return['options']->powertrain,
						'handiness_desired' => $return['options']->handiness,
						'reliability_desired' => $return['options']->reliability,
						'multimedia_desired' => $return['options']->multimedia
					);
					// print_r($return['options']);
					$arrayForReturn->compatibility = $this->cococaralgorithm->calculateAllCoefficients($arrayDesired);
					// $arrayForPush['coefficients'] = $this->cococaralgorithm->returnArrayFit();
					# -----					
					array_push($arrayForReturnForPush, $arrayForReturn);
				}

				$return['design'] = $arrayForReturnForPush;
			}
		}

		return $return;
	}

	public function retrievealltestdrive()
	{
		$return = array();
		$this->db->from('customer_appointment AS appointment');
		// $this->db->join('customer_appointment_has_car AS hascar','hascar.customer_appointment_idcustomer_appointment = appointment.idcustomer_appointment');
		// $this->db->join('car', 'car.idcar = hascar.car_idcar');
		$this->db->join('customer_information AS customer', 'customer.idcustomer_information = appointment.customer_information_idcustomer_information');
		$this->db->order_by("idcustomer_appointment", "desc");
		$return = $this->db->get()->result();

		return $return;
	}

	public function retrievetestdrive($idcustomer_appointment = NULL)
	{
		$arrayData = array();
		$returnMakes = array();
		$arrayForReturnForPush = array();
		$return = array();
		$Return = array();
		$this->db->select('idcustomer_information, customer.fullname, customer.email, customer.phone, customer.message, appointment.appointmentdate, 
			coid, 
			options.price AS oprice, 
			options.passengers AS opassengers, 
			options.make AS omake,
			options.model AS omodel, 
			options.bodytype AS otype, 
			options.generation AS ogeneration,
			options.safety AS osafety,
			options.fuelconsumption AS ofuel,
			options.space AS ospace,
			options.comfort AS ocomfort,
			options.powertrain AS opower,
			options.handiness AS ohandling,
			options.reliability AS oreliable,
			options.multimedia AS omedia,
			car.make, car.model, car.engine, car.modelvariant, car.equipment, car.generation, body, seats,propulsion, power, fuel, car.fueltype,
			car.safety,
			car.fuelcons,
			car.space,
			car.comfort,
			car.environment,
			car.powertrain,
			car.driving,
			car.handiness,
			car.reliability,
			car.multimedia,
			hascar.percentage
			');
		$this->db->where('idcustomer_appointment', $idcustomer_appointment);
		$this->db->from('customer_appointment AS appointment');
		$this->db->join('customer_appointment_has_car AS hascar','hascar.customer_appointment_idcustomer_appointment = appointment.idcustomer_appointment');
		$this->db->join('car', 'car.idcar = hascar.car_idcar');
		$this->db->join('customer_information AS customer', 'customer.idcustomer_information = appointment.customer_information_idcustomer_information');
		$this->db->join('customer_options AS options', 'options.customer_information_idcustomer_information = customer.idcustomer_information');
		$this->db->order_by("idcustomer_appointment", "desc");
		$return = $this->db->get()->row();


		// get the makes tables, if naa
		if (!empty($return->idcustomer_information)) {
			$this->db->join('car_makes AS makes', 'makes.idcar_makes = customer.car_makes_idcar_makes');
			$this->db->where('customer_information_idcustomer_information', $return->idcustomer_information);
			$this->db->from('customer_makes as customer');
			$returnMakes = $this->db->get()->result();
			if (count($returnMakes) > 0) {
				$Return['makes'] = $returnMakes;
			}
		}

		// get the yourdesigns tables, if naa
		if (!empty($return->idcustomer_information)) {
			$this->db->join('car', 'car.idcar = design.car_idcar');
			$this->db->where('customer_information_idcustomer_information', $return->idcustomer_information);
			$this->db->from('customer_yourdesign as design');
			$returnDesign = $this->db->get()->result();
			if (count($returnDesign) > 0) {
				foreach ($returnDesign as $value) {
					$arrayForReturn = $value;

					# ---- compute compatiblity
					$arrayDb = array(
						'safety_db' => $value->safety,
			 			'fuel_cons_db' => $value->fuelcons,
			 			'space_db' => $value->space,
			 			'comfort_db' => $value->comfort,
			 			'powertrain_db' => $value->powertrain,
			 			'handniness_db' => $value->handiness,
			 			'reliability_db' => $value->reliability,
			 			'multimedia_db' => $value->multimedia
					);
					$this->cococaralgorithm->populateFromDb($arrayDb);
					$arrayDesired = array(
						'safety_desired' => $return->osafety,
						'fuel_cons_desired' => $return->ofuel,
						'space_desired' => $return->ospace,
						'comfort_desired' => $return->ocomfort,
						'powertrain_desired' => $return->opower,
						'handiness_desired' => $return->ohandling,
						'reliability_desired' => $return->oreliable,
						'multimedia_desired' => $return->omedia
					);
					// print_r($return['options']);
					$arrayForReturn->compatibility = $this->cococaralgorithm->calculateAllCoefficients($arrayDesired);
					// $arrayForPush['coefficients'] = $this->cococaralgorithm->returnArrayFit();
					# -----					
					array_push($arrayForReturnForPush, $arrayForReturn);
				}

				$Return['design'] = $arrayForReturnForPush;
			}
		}
		$Return['testdrive-contact'] = $return;
		return $Return;
	}

	
}