<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marken_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('cococaralgorithm');
	}

	public function getLogo($whosactive = array())
	{
		$arrayResult = array();
		$arrayToPush = array();
		$this->db->from('car_makes AS make');
		$this->db->where('make.isactive', TRUE);

		$result = $this->db->get()->result();

		foreach ($result as $value) {
			if (count($whosactive > 0)) {
				if (in_array(strtolower($value->maker), $whosactive)) {
					$arrayToPush['inTheArray'] = true;
				} else {
					$arrayToPush['inTheArray'] = false;
				}
			} else {
				$arrayToPush['inTheArray'] = false;
			}
			$arrayToPush['logoInformation'] = $value;

			array_push($arrayResult, $arrayToPush);
		}

		return $arrayResult;
	}
}