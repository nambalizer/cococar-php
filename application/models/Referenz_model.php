<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referenz_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->carmakes = "car_makes";
		$this->currentDate = date('Y-m-d H:i:s');
	}

	public function showCarMakes()
	{
		$this->db->where('isactive', TRUE);
		return $this->db->get($this->carmakes)->result();
	}

	public function showAllMake($withLogo = false)
	{
		$this->db->from('car');
		
		if ($withLogo == TRUE) {
			$this->db->select('make, iconurl, logourl');
			$this->db->join('car_makes AS carmakes', 'carmakes.idcar_makes = car.car_makes_idcar_makes');
		} else
			$this->db->select('make');

		$this->db->group_by("make"); 
		$this->db->order_by('make', 'ASC');
		$this->db->where('car.isactive', TRUE);

		return $this->db->get()->result();
	}

	public function showAllModel($idcar = NULL, $withIcon = false)
	{
		if (!empty($idcar)) {
			$this->db->select('model');
			$this->db->from('car');
			$this->db->where('isactive', TRUE);
			$this->db->where('car_makes_idcar_makes', $idcar);
			$this->db->group_by("model"); 
			$this->db->order_by('model', 'ASC');

			return $this->db->get()->result();
		} else {
			return false;
		}
	}

	public function rightIconForModel($make = NULL, $model = NULL)
	{

	}

	public function showAllBodyType($model = NULL, $idcar = NULL)
	{
		if (!empty($model)) {
			$this->db->select('body');
			$this->db->from('car');
			$this->db->where('isactive', TRUE);
			$this->db->where('model', $model);
			$this->db->where('car_makes_idcar_makes', $idcar);
			$this->db->group_by('body');
			$this->db->order_by('body', 'ASC');

			return $this->db->get()->result();
		} else {
			return false;
		}
	}

	public function showAllGeneration($bodytype, $model, $makeid)
	{
		if (!empty($bodytype)) {
			$this->db->select('generation, car_makes_idcar_makes');
			$this->db->from('car');
			$this->db->where('isactive', TRUE);
			$this->db->where('body', $bodytype);
			$this->db->where('model', $model);
			$this->db->where('car_makes_idcar_makes', $makeid);
			$this->db->group_by('generation');
			$this->db->order_by('generation', 'ASC');

			return $this->db->get()->result();
		} else {
			return false;
		}
	}

	public function showthecar($generation, $bodytype, $model, $makeid)
	{
		if (!empty($generation) && !empty($bodytype) && !empty($model) && !empty($makeid)) {
			$this->db->select('idcar, make, model, body, car_makes_idcar_makes');
			$this->db->from('car');
			$this->db->where('isactive', TRUE);
			$this->db->where('body', $bodytype);
			$this->db->where('model', $model);
			$this->db->where('car_makes_idcar_makes', $makeid);

			$result = $this->db->get()->row();

			# get the picture now
			$this->db->where('has.car_idcar', $result->idcar);
			$this->db->from('car_picture AS picture');
			$this->db->join('car_has_car_picture AS has', 'has.car_picture_idcar_picture = picture.idcar_picture');

			$result->carpicturearray = $this->db->get()->row();

			return $result;

		} else {
			return false;
		}
	}

	public function getPicture($carid)
	{
		if (!empty($carid)) { 
			$this->db->from('car_picture');
			$this->db->where('isactive', TRUE);
		}
	}

	public function getMoreInformationMake($carmaker)
	{
		if (!empty($carmaker)){
			$this->db->from('car_makes');
			$this->db->where('isactive', TRUE);
			$this->db->where('idcar_makes', $carmaker);
			

			return $this->db->get()->row();
		} else {
			return;
		}
	}

	public function getIconOfTheCar($idcar_makes) {
		$this->db->where('idcar_makes',$idcar_makes);
		$this->db->from('car_makes');

		return $this->db->get()->row();
	}

	public function getMoreInformationModel($carmodel)
	{
		# retrieve more information such as the icon
	}

	public function getMoreInformationType($carBodyType)
	{

	}

	public function getMoreInformationGeneration($cargeneration)
	{

	}


}

?>