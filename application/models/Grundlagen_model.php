<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grundlagen_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->currentDate = date('Y-m-d H:i:s');
	}
}