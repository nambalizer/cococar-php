<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wunschauto_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->car = "car";
		$this->currentDate = date('Y-m-d H:i:s');
		$this->load->library('cococaralgorithm');

	}

	public function getRatings($idcar, $round = false)
	{
		$arrayData = array();
		$this->db->where('idcar', $idcar);
		$result = $this->db->get($this->car)->row();

		if ($round == true) {
			$result->safety = floor($result->safety);
			$result->fuelcons = floor($result->fuelcons);
			$result->space = floor($result->space);
			$result->comfort = floor($result->comfort);
			$result->powertrain = floor($result->powertrain);
			$result->handiness = floor($result->handiness);
			$result->reliability = floor($result->reliability);
			$result->multimedia = floor($result->multimedia);
			$result->pointsconsumed = $result->safety + $result->fuelcons + $result->space + $result->comfort + $result->powertrain + $result->handiness + $result->reliability + $result->multimedia;
		}
		
		return $result;
	}
}