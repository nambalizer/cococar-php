<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() 
	{
		parent::__construct();
		
	}

	
	public function index()
	{
		// $this->load->view('welcome_message');
		// redirect(base_url().'underconstruction', 'refresh');
		// redirect('http://13.76.32.145/home/','refresh');
		$ratio = filter_var($this->input->get('ratio'), FILTER_SANITIZE_STRING);
		$mobile = filter_var($this->input->get('mobile'), FILTER_SANITIZE_STRING);


		$display = array(
			'page-title' => 'Willkommen zu', // <title>
			'what-process' =>  'Deine Design', // breadcrumbs, h2
			'what-nav' => 0,
			'active-page' => 'home',
			'what-step' => 2.75 // <nav>
		);

		if ($ratio == 2){

		} else {
			$this->load->view(
		        'templates/homepage.phtml', array(
	            'display' => $display,
	            'mobile' => $mobile,
	            'ratio' => $ratio,
	            'view' => 'templates/homepage/index',
	            'viewjs' => 'templates/homepage/index-js'
	        )); 	
		}		

        $this->lang->line('welcome_message');
	}

	// for the video
	// remove gif if screen is desktop
	public function removegif()
	{

	}

	public function screentest()
	{
		$ratio = filter_var($this->input->get('ratio'), FILTER_SANITIZE_STRING);
		$mobile = filter_var($this->input->get('mobile'), FILTER_SANITIZE_STRING);
		// echo 'ratio:'.urldecode(@$ratio);

		$display = array(
			'page-title' => 'Willkommen zu', // <title>
			'what-process' =>  'Deine Design', // breadcrumbs, h2
			'what-nav' => 0,
			'active-page' => 'home',
			'what-step' => 2.75 // <nav>
		);

		
			$this->load->view(
		        'templates/homepage.phtml', array(
	            'display' => $display,
	            'mobile' => $mobile,
	            'ratio' => $ratio,
	            'view' => 'templates/homepage/index',
	            'viewjs' => 'templates/homepage/screentest-js'
	        )); 	
			
	}
}
