$(document).ready(function(e) 
{
	// var pointsTotal = 27
	// var pointsSicherheit = 0;  //to be changed by PHP
	// var pointsVerbrauch = 0;
	// var pointsPlatzangebot = 0;
	// var pointsKomfort = 0;
	// var pointsAntrieba = 0;
	// var pointsHandlichkeit = 0;
	// var pointsZuverlassigkeit = 0;
	// var pointsMultimedia = 0;

	// points-sicherheit
	// points-verbrauch
	// points-platzangebot
	// points-komfort
	// points-antrieb
	// points-handlichkeit
	// points-zuverlassigkeit

	// sicherheit-*
	// verbrauch-*
	// platzangebot-*
	// komfort-*
	// antrieba-*
	// handlichkeit-*
	// zuverlassigkeit-*
	// multimedia-*

	// btn-sicherheit
	// btn-verbrauch
	// btn-platzangebot
	// btn-komfort
	// btn-antrieb
	// btn-handlichkeit
	// btn-zuverlassigkeit
	// btn-multimedia

	// modified just to change the message of bitbucket.

	
	jQuery('#points-sicherheit').val(pointsSicherheit);
	jQuery('#points-verbrauch').val(pointsVerbrauch);
	jQuery('#points-platzangebot').val(pointsPlatzangebot);
	jQuery('#points-komfort').val(pointsKomfort);
	jQuery('#points-antrieb').val(pointsAntrieba);
	jQuery('#points-handlichkeit').val(pointsHandlichkeit);
	jQuery('#points-zuverlassigkeit').val(pointsZuverlassigkeit);
	jQuery('#points-multimedia').val(pointsMultimedia);
	jQuery('#cicle-shape-number').html(pointsTotal);

	justanimate('#sicherheit', pointsSicherheit);
	justanimate('#verbrauch', pointsVerbrauch);
	justanimate('#platzangebot', pointsPlatzangebot);
	justanimate('#komfort', pointsKomfort);
	justanimate('#antrieba', pointsAntrieba);
	justanimate('#handlichkeit', pointsHandlichkeit);
	justanimate('#zuverlassigkeit', pointsZuverlassigkeit);
	justanimate('#multimedia', pointsMultimedia);
	


	changepoints('#points-sicherheit', pointsSicherheit);
	colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
	changepoints('#points-verbrauch', pointsVerbrauch);
	colorizeSlider('#points-verbrauch','.btn-verbrauch','#verbrauch');
	changepoints('#points-platzangebot', pointsPlatzangebot);
	colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
	changepoints('#points-komfort', pointsKomfort);
	colorizeSlider('#points-komfort','.btn-komfort','#komfort');
	changepoints('#points-antrieb', pointsAntrieba);
	colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
	changepoints('#points-handlichkeit', pointsHandlichkeit);
	colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
	changepoints('#points-zuverlassigkeit', pointsZuverlassigkeit);
	colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
	changepoints('#points-multimedia', pointsMultimedia);
	colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');

	jQuery('#cicle-shape-number').html(28);
	// justanimatecenter(28, pointsTotal);
	// Function to update counters on all elements with class counter
	  var doUpdate = function() {
	    $('#cicle-shape-number').each(function() {
	      var count = parseInt($(this).html());
	      if (count !== pointsTotal) {
	        $(this).html(count - 1);
	      }
	    });
	  };

	  // Schedule the update to happen once every second
	  setInterval(doUpdate, 200);


	// jQuery('#cicle-shape-number').delay(1100).html(28);
	// console.log(totalall());

	jQuery("#sicherheit-1" ).on( "click", function handler() 
	{
		if (jQuery('#points-sicherheit').val() == 1){
			changepoints('#points-sicherheit', 0);
		} else {
			changepoints('#points-sicherheit', 1);
		}

		colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
		
	});
	jQuery("#sicherheit-2" ).on( "click", function handler() 
	{
		changepoints('#points-sicherheit', 2);
		colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
		
	});
	jQuery("#sicherheit-3" ).on( "click", function handler() 
	{
		changepoints('#points-sicherheit', 3);
		colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
		
	});
	jQuery("#sicherheit-4" ).on( "click", function handler() 
	{
		changepoints('#points-sicherheit', 4);
		colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
		
	});
	jQuery('#sicherheit-5').on("click", function handler()
	{
		changepoints('#points-sicherheit', 5);
		colorizeSlider('#points-sicherheit','.btn-sicherheit','#sicherheit');
		
	});	

	jQuery('#verbrauch-1').on("click", function handler()
	{
		if (jQuery('#points-verbrauch').val() == 1){
			changepoints('#points-verbrauch', 0);
		} else {
			changepoints('#points-verbrauch', 1);
		}
		colorizeSlider('#points-verbrauch', '.btn-verbrauch', '#verbrauch');

	});
	jQuery('#verbrauch-2').on("click", function handler()
	{
		changepoints('#points-verbrauch', 2);
		colorizeSlider('#points-verbrauch','.btn-verbrauch','#verbrauch');
		
	});
	jQuery('#verbrauch-3').on("click", function handler()
	{
		changepoints('#points-verbrauch', 3);
		colorizeSlider('#points-verbrauch','.btn-verbrauch','#verbrauch');
		
	});
	jQuery('#verbrauch-4').on("click", function handler()
	{
		changepoints('#points-verbrauch', 4);
		colorizeSlider('#points-verbrauch','.btn-verbrauch','#verbrauch');
		
	});
	jQuery('#verbrauch-5').on("click", function handler()
	{
		changepoints('#points-verbrauch', 5);
		colorizeSlider('#points-verbrauch','.btn-verbrauch','#verbrauch');
		
	});
	
	jQuery('#platzangebot-1').on("click", function handler()
	{
		if (jQuery('#points-platzangebot').val() == 1) {
			changepoints('#points-platzangebot', 0);
		} else {
			changepoints('#points-platzangebot', 1);
		}
		colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
	});
	jQuery('#platzangebot-2').on("click", function handler()
	{
		changepoints('#points-platzangebot', 2);
		colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
		
	});
	jQuery('#platzangebot-3').on("click", function handler()
	{
		changepoints('#points-platzangebot', 3);
		colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
		
	});
	jQuery('#platzangebot-4').on("click", function handler()
	{
		changepoints('#points-platzangebot', 4);
		colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
		
	});
	jQuery('#platzangebot-5').on("click", function handler()
	{
		changepoints('#points-platzangebot', 5);
		colorizeSlider('#points-platzangebot','.btn-platzangebot','#platzangebot');
		
	});

	jQuery('#komfort-1').on("click", function handler()
	{
		if (jQuery('#points-komfort').val() == 1) {
			changepoints('#points-komfort', 0);
		} else {
			changepoints('#points-komfort', 1);
		}
		colorizeSlider('#points-komfort','.btn-komfort','#komfort');
	});
	jQuery('#komfort-2').on("click", function handler()
	{
		changepoints('#points-komfort', 2);
		colorizeSlider('#points-komfort','.btn-komfort','#komfort');
		
	});
	jQuery('#komfort-3').on("click", function handler()
	{
		changepoints('#points-komfort', 3);
		colorizeSlider('#points-komfort','.btn-komfort','#komfort');
		
	});
	jQuery('#komfort-4').on("click", function handler()
	{
		changepoints('#points-komfort', 4);
		colorizeSlider('#points-komfort','.btn-komfort','#komfort');
		
	});
	jQuery('#komfort-5').on("click", function handler()
	{
		changepoints('#points-komfort', 5);
		colorizeSlider('#points-komfort','.btn-komfort','#komfort');
		
	});

	jQuery('#antrieba-1').on("click", function handler()
	{
		if (jQuery('#points-antrieb').val() == 1) {
			changepoints('#points-antrieb', 0);
		} else {
			changepoints('#points-antrieb', 1);
		}
		colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
		
	});
	jQuery('#antrieba-2').on("click", function handler()
	{
		changepoints('#points-antrieb', 2);
		colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
		
	});
	jQuery('#antrieba-3').on("click", function handler()
	{
		changepoints('#points-antrieb', 3);
		colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
		
	});
	jQuery('#antrieba-4').on("click", function handler()
	{
		changepoints('#points-antrieb', 4);
		colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
		
	});
	jQuery('#antrieba-5').on("click", function handler()
	{
		changepoints('#points-antrieb', 5);
		colorizeSlider('#points-antrieb','.btn-antrieb','#antrieba');
		
	});

	jQuery('#handlichkeit-1').on("click", function handler()
	{
		if (jQuery('#points-handlichkeit').val() == 1) {
			changepoints('#points-handlichkeit', 0);
		} else {
			changepoints('#points-handlichkeit', 1);
		}
		
		colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
		
	});
	jQuery('#handlichkeit-2').on("click", function handler()
	{
		changepoints('#points-handlichkeit', 2);
		colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
		
	});
	jQuery('#handlichkeit-3').on("click", function handler()
	{
		changepoints('#points-handlichkeit', 3);
		colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
		
	});
	jQuery('#handlichkeit-4').on("click", function handler()
	{
		changepoints('#points-handlichkeit', 4);
		colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
		
	});
	jQuery('#handlichkeit-5').on("click", function handler()
	{
		changepoints('#points-handlichkeit', 5);
		colorizeSlider('#points-handlichkeit','.btn-handlichkeit','#handlichkeit');
		
	});

	jQuery('#zuverlassigkeit-1').on("click", function handler()
	{
		if (jQuery('#points-zuverlassigkeit').val() == 1) {
			changepoints('#points-zuverlassigkeit', 0);
		} else {
			changepoints('#points-zuverlassigkeit', 1);
		}
		colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
		
	});
	jQuery('#zuverlassigkeit-2').on("click", function handler()
	{
		changepoints('#points-zuverlassigkeit', 2);
		colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
		
	});
	jQuery('#zuverlassigkeit-3').on("click", function handler()
	{
		changepoints('#points-zuverlassigkeit', 3);
		colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
		
	});
	jQuery('#zuverlassigkeit-4').on("click", function handler()
	{
		changepoints('#points-zuverlassigkeit', 4);
		colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
		
	});
	jQuery('#zuverlassigkeit-5').on("click", function handler()
	{
		changepoints('#points-zuverlassigkeit', 5);
		colorizeSlider('#points-zuverlassigkeit','.btn-zuverlassigkeit','#zuverlassigkeit');
		
	});

	jQuery('#multimedia-1').on("click", function handler()
	{
		if (jQuery('#points-multimedia').val() == 1) {
			changepoints('#points-multimedia', 0);	
		} else {
			changepoints('#points-multimedia', 1);
		}
		
		colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');
		
	});
	jQuery('#multimedia-2').on("click", function handler()
	{
		changepoints('#points-multimedia', 2);
		colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');
		
	});
	jQuery('#multimedia-3').on("click", function handler()
	{
		changepoints('#points-multimedia', 3);
		colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');
		
	});
	jQuery('#multimedia-4').on("click", function handler()
	{
		changepoints('#points-multimedia', 4);
		colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');
		
	});
	jQuery('#multimedia-5').on("click", function handler()
	{
		changepoints('#points-multimedia', 5);
		colorizeSlider('#points-multimedia','.btn-multimedia','#multimedia');
		
	});


	function colorizeSlider(whatslider,classbtn,btnid)
	{
		// btn-default - empty
		// btn-danger - if 1 ang slider
		// btn-yellow - if 2 lang ang slider
		// btn-warning - if 3 lang ang slider
		// btn-green - if 4 or 5
		jQuery(classbtn).removeClass("btn-green").addClass("btn-default");
		jQuery(classbtn).removeClass("btn-warning").addClass("btn-default");
		jQuery(classbtn).removeClass("btn-yellow").addClass("btn-default");
		jQuery(classbtn).removeClass("btn-danger").addClass("btn-default");

		if (parseInt(jQuery(whatslider).val()) == 5) {
			jQuery(classbtn).removeClass("btn-default").addClass("btn-green", 1000);
		} else if (parseInt(jQuery(whatslider).val()) == 4) {
			jQuery(classbtn).removeClass("btn-default").addClass("btn-green", 1000);
			jQuery(btnid+'-5').removeClass("btn-green").addClass("btn-default", 1000);
		} else if (parseInt(jQuery(whatslider).val()) == 3) {
			jQuery(classbtn).removeClass("btn-default").addClass("btn-warning", 1000);
			jQuery(btnid+'-5').removeClass("btn-warning").addClass("btn-default", 1000);
			jQuery(btnid+'-4').removeClass("btn-warning").addClass("btn-default", 1000);
		} else if (parseInt(jQuery(whatslider).val()) == 2) {
			jQuery(classbtn).removeClass("btn-default").addClass("btn-warning", 1000);
			jQuery(btnid+'-5').removeClass("btn-warning").addClass("btn-default", 1000);
			jQuery(btnid+'-4').removeClass("btn-warning").addClass("btn-default", 1000);
			jQuery(btnid+'-3').removeClass("btn-warning").addClass("btn-default", 1000);
		} else if (parseInt(jQuery(whatslider).val()) == 1) {
			jQuery(classbtn).removeClass("btn-default").addClass("btn-danger", 1000);
			jQuery(btnid+'-5').removeClass("btn-danger").addClass("btn-default", 1000);
			jQuery(btnid+'-4').removeClass("btn-danger").addClass("btn-default", 1000);
			jQuery(btnid+'-3').removeClass("btn-danger").addClass("btn-default", 1000);
			jQuery(btnid+'-2').removeClass("btn-danger").addClass("btn-default", 1000);
		}


		if (pointsTotal == 0)
		{
			_toastr("Keine weiteren Punkte links","bottom-center","error",false);
			jQuery('.whitecircle').removeClass("whitecircle").addClass("redcircle");
		} else {
			jQuery('.redcircle').removeClass("redcircle").addClass("whitecircle");
		}
	}

	function changepoints(idbtn, lvlbtn)
	{
		if (pointsTotal >= 5) {
			pointsTotal = pointsReduction(idbtn, lvlbtn);
			jQuery('#cicle-shape-number').html(pointsTotal);
			jQuery(idbtn).val(lvlbtn);
			return true;
		} else if (pointsTotal == lvlbtn) {
			pointsTotal = pointsReduction(idbtn, lvlbtn);
			jQuery('#cicle-shape-number').html(pointsTotal);
			jQuery(idbtn).val(lvlbtn);
			return true;
		} else if ((pointsTotal > 0) && (pointsTotal < lvlbtn))	{
			// if (4 > 0) && (4 < 5)
			// if (3 > 0) && (3 < 4)
			// if (3 > 0) && (3 < 5)
			// if (2 > 0) && (2 < 3)
			// if (2 > 0) && (2 < 4)
			// if (2 > 0) && (2 < 5)
			// if (1 > 0) && (1 < 2)
			// if (1 > 0) && (1 < 3)
			// if (1 > 0) && (1 < 4)
			// if (1 > 0) && (1 < 5)			
			if (jQuery(idbtn).val() != 0) {
				if (jQuery(idbtn).val() < lvlbtn)
				{
					pointsTotal = pointsReduction(idbtn, lvlbtn);
					if (pointsTotal >= 0) {
						jQuery(idbtn).val(lvlbtn);
					} else {
						jQuery(idbtn).val(lvlbtn - Math.abs(pointsTotal));
						pointsTotal = 0;
					}
					jQuery('#cicle-shape-number').html(pointsTotal);
				}
			}
			else {
				jQuery(idbtn).val(pointsTotal);
				pointsTotal = 0;
				jQuery('#cicle-shape-number').html(pointsTotal);
			}		
			return true;
		} else if ((pointsTotal > 0) && (pointsTotal > lvlbtn))	{
			// if (4 > 0) && (4 > 3)
			// if (4 > 0) && (4 > 2)
			// if (4 > 0) && (4 > 1)
			// if (3 > 0) && (3 > 2)
			// if (3 > 0) && (3 > 1)
			// if (2 > 0) && (2 > 1)

			// if (jQuery(idbtn).val() != 0) {
				pointsTotal = pointsReduction(idbtn, lvlbtn);
				jQuery(idbtn).val(lvlbtn);
				jQuery('#cicle-shape-number').html(pointsTotal);
			// } else {

			// }
		} else if (pointsTotal == 0) {
			if (jQuery(idbtn).val() > lvlbtn){
				pointsTotal = pointsReduction(idbtn, lvlbtn);
				jQuery(idbtn).val(lvlbtn);
			}
			else {}

			
			jQuery('#cicle-shape-number').html(pointsTotal);
		} else 	{
			return false;
		}		
	}

	function pointsReduction(idbtn, lvlbtn)
	{
		var newvalue = 0;
		var pointsTotaltemp = 0;
		if (jQuery(idbtn).val() > lvlbtn) {
			newvalue = jQuery(idbtn).val() - lvlbtn;
			pointsTotaltemp =  pointsTotal + newvalue;
		} else if (jQuery(idbtn).val() < lvlbtn) {
			newvalue = lvlbtn - jQuery(idbtn).val();
			pointsTotaltemp = pointsTotal - newvalue;
		} else if (jQuery(idbtn).val() == lvlbtn) {
			return pointsTotal;
		}
		else{ }

		return pointsTotaltemp;
	}

	function totalall()
	{
		var testvalue1 = parseInt(jQuery('#points-sicherheit').val());
		var testvalue2 = parseInt(jQuery('#points-verbrauch').val());
		var testvalue3 = parseInt(jQuery('#points-platzangebot').val());
		var testvalue4 = parseInt(jQuery('#points-komfort').val());
		var testvalue5 = parseInt(jQuery('#points-antrieb').val());
		var testvalue6 = parseInt(jQuery('#points-handlichkeit').val());
		var testvalue7 = parseInt(jQuery('#points-zuverlassigkeit').val())
		var testvalue8 = parseInt(jQuery('#points-multimedia').val());


		return pointsTotal+testvalue8+testvalue7+testvalue6+testvalue5+testvalue4+testvalue3+testvalue2+testvalue1;
	}

	function justanimate(btnid, points)
	{
		// btn-default - empty
		// btn-danger - if 1 ang slider
		// btn-yellow - if 2 lang ang slider
		// btn-warning - if 3 lang ang slider
		// btn-green - if 4 or 5

		var counter = 1;
		var counterdelay = 400;

		while(counter <= points) {
			jQuery( btnid+"-"+counter ).delay(counterdelay).switchClass( "btn-default", "btn-default", 1, "slow" );
			jQuery( btnid+"-"+counter ).delay(counterdelay).switchClass( "btn-danger", "btn-default", 1000000, "slow" );
			jQuery( btnid+"-"+counter ).delay(counterdelay).switchClass( "btn-yellow", "btn-default", 1000000, "slow" );
			jQuery( btnid+"-"+counter ).delay(counterdelay).switchClass( "btn-warning", "btn-default", 1000000, "slow" );
			jQuery( btnid+"-"+counter ).delay(counterdelay).switchClass( "btn-green", "btn-default", 1000000, "slow" );	
			counter++;
			counterdelay = counterdelay + 100;
		}
	}


	// 
	// handlichkeit-*
	// zuverlassigkeit-*
	// multimedia-*
});