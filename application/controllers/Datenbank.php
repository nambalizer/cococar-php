<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datenbank extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('referenz_model', 'referenzmodel');
		$this->load->model('result_model', 'resultmodel');
		$this->load->model('design_model', 'designmodel');
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');

		
	}

	
	public function index()
	{
		//
        $arrayData = array();
		$error = false;
		$errorMessage = 'Please check for error';

		$arrayData['allmakes'] = $this->referenzmodel->showCarMakes();



		if ($_POST) {
			$this->form_validation->set_rules('carmaker', 'Car Maker', 'trim|required');
        	$this->form_validation->set_rules('carmodel', 'Car Model', 'trim|required');
        	$this->form_validation->set_rules('carbodytype', 'Car Body type', 'trim|required');
        	$this->form_validation->set_rules('cargeneration', 'Car Generation', 'trim|required');

        	$arrayData['referenz']['carmaker'] = ucwords(filter_var($this->input->post('carmaker'), FILTER_SANITIZE_STRING));
        	$arrayData['referenz']['carmodel'] = ucwords(filter_var($this->input->post('carmodel'), FILTER_SANITIZE_STRING));
        	$arrayData['referenz']['carbodytype']= ucwords(filter_var($this->input->post('carbodytype'), FILTER_SANITIZE_STRING));
        	$arrayData['referenz']['cargeneration'] = ucwords(filter_var($this->input->post('cargeneration'), FILTER_SANITIZE_STRING));

        	if (strcasecmp($arrayData['referenz']['carmaker'], '')==0) {
        		$error = true;
        		$arrayData['errorMessage'] = 'Please check car make';
				
        	} else {
        		$error = false;
        	}

        	if (strcasecmp($arrayData['referenz']['carmodel'], '')==0) {
        		$error = true;
        		$arrayData['errorMessage'] = 'Please check car model';
        	} else {
        		$error = false;
        	}

        	if ($error == true) {
        		$errorsession = validation_errors(); 
				// $this->session->unset_userdata('errorsession');
				// $this->session->set_userdata('errorsession', $errorsession);
        	} else {
        		$arrayData['uiPrice'] = $this->advisorysession['uiPrice'];
        		$arrayData['uiPassenger'] = $this->advisorysession['uiPassenger'];

	        	if ( (!empty($arrayData['referenz']['carmaker'])) &&
					(!empty($arrayData['referenz']['carmodel'])) &&
					(!empty($arrayData['referenz']['carbodytype'])) &&
					(!empty($arrayData['referenz']['cargeneration'])) ) {
					$arrayData['referenz']['carselected'] = $this->referenzmodel->showthecar($arrayData['referenz']['cargeneration'], $arrayData['referenz']['carbodytype'], $arrayData['referenz']['carmodel'], $arrayData['referenz']['carmaker']);
				}

        		// $this->session->set_userdata('advisorysession', $arrayData);
        		// print_r($arrayData);
        		redirect(base_url('datenbank/mehrinformationen?idcar='.$arrayData['referenz']['carselected']->idcar), 'refresh');
        	}

		} else {
			// do the session instead
			if (!empty($this->advisorysession)) {
				$arrayData['uiPrice'] = $this->advisorysession['uiPrice'];
		    	$arrayData['uiPassenger'] = $this->advisorysession['uiPassenger'];
		    	if (!empty($this->advisorysession['referenz'])) {
					$arrayData['referenz']['carmaker'] = $this->advisorysession['referenz']['carmaker'];
			    	$arrayData['referenz']['carmodel'] = $this->advisorysession['referenz']['carmodel'];
			    	$arrayData['referenz']['carbodytype']= $this->advisorysession['referenz']['carbodytype'];
			    	$arrayData['referenz']['cargeneration'] = $this->advisorysession['referenz']['cargeneration'];
			    }
		    }
		}

		if (!empty($arrayData['referenz']['carmaker'])) {
			$arrayData['referenz']['carmaker'] = $this->referenzmodel->getMoreInformationMake($arrayData['referenz']['carmaker']);
		}

		if (!empty($arrayData['referenz']['carmodel'])) {
			// $arrayData['referenz']['carmodel'] = $this->referenzmodel->getMoreInformationModel($arrayData['referenz']['carmodel']);
		}

		if (!empty($arrayData['referenz']['carbodytype'])) {
			// $arrayData['referenz']['carbodytype'] = $this->referenzmodel->getMoreInformationType($arrayData['referenz']['carbodytype']);	
		}
		if (!empty($arrayData['referenz']['cargeneration'])) {
			// $arrayData['referenz']['cargeneration'] = $this->referenzmodel->getMoreInformationGeneration($arrayData['referenz']['cargeneration']);
		}

		if ( (!empty($arrayData['referenz']['carmaker']->idcar_makes)) &&
			(!empty($arrayData['referenz']['carmodel'])) &&
			(!empty($arrayData['referenz']['carbodytype'])) &&
			(!empty($arrayData['referenz']['cargeneration'])) ) {
			$arrayData['referenz']['carselected'] = $this->referenzmodel->showthecar($arrayData['referenz']['cargeneration'], $arrayData['referenz']['carbodytype'], $arrayData['referenz']['carmodel'], $arrayData['referenz']['carmaker']->idcar_makes);
		}

		// $display = array(
		// 	'page-title' => 'Advisory Process', // <title>
		// 	'what-process' =>  'Deine Grundlagen', // breadcrumbs, h2
		// 	'what-nav' => 0,
		// 	'active-page' => '',
		// 	'what-step' => 1.5 // <nav>
		// );



		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Deine Grundlagen', // breadcrumbs, h2
			'what-nav' => 0,
			'what-step' => 1.75,
			'active-page' => '', // <nav>
			'arrayData' => $arrayData
		);


		$this->load->view(
        'templates/datenbank/template.phtml', array(
            'display' => $display,
            'view' => 'templates/datenbank/index',
            'viewjs' => 'templates/datenbank/index-js'
        )); 
	

		

        $this->lang->line('welcome_message');


	}

	public function changemodeloption()
	{
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);
		$display = $this->referenzmodel->showAllModel($tags);


		$this->load->view('templates/option/bodymodel.phtml', array('display' => $display)); 
	}

	public function changebodytype()
	{
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);
		$makeid = filter_var($this->input->get('makeid'), FILTER_SANITIZE_STRING);
		
		$display = $this->referenzmodel->showAllBodyType($tags, $makeid);

		$this->load->view('templates/option/bodytype.phtml', array('display' => $display)); 	
	}

	public function changecargeneration()
	{
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);
		$makeid = filter_var($this->input->get('makeid'), FILTER_SANITIZE_STRING);
		$model = filter_var($this->input->get('model'), FILTER_SANITIZE_STRING);

		$display = $this->referenzmodel->showAllGeneration($tags, $model, $makeid);
		$this->load->view('templates/option/carsgeneration.phtml', array('display' => $display)); 		
	}

	public function showCar()
	{
		// http://localhost:6060/deinereferenz/showCar?tags=VI%20FL1&cartype=Limousine&makeid=2&model=5er
		$tags = filter_var($this->input->get('tags'), FILTER_SANITIZE_STRING);
		$makeid = filter_var($this->input->get('makeid'), FILTER_SANITIZE_STRING);
		$model = filter_var($this->input->get('model'), FILTER_SANITIZE_STRING);
		$bodytype = filter_var($this->input->get('cartype'), FILTER_SANITIZE_STRING);

		$display = $this->referenzmodel->showthecar($tags, $bodytype, $model, $makeid);

		if (!empty($display))
			$displaymake = $this->referenzmodel->getIconOfTheCar($display->car_makes_idcar_makes); //car_makes_idcar_makes

		if (isset($displaymake) && count($displaymake) > 0)
			$this->load->view('templates/advisoryprocess/deine-referenz-result.phtml', array('display' => $display, 'displaymake' => $displaymake));
		else 
			$this->load->view('templates/advisoryprocess/deine-referenz-result.phtml', array('display' => $display));
	}

	public function mehrinformationen()
	{
		$error = false;
		$errorMessage = 'Please check for error';
		$arraySelectedCar = array();
		$dataarray  = array();
		$redirect = false;

        $idcar = filter_var($this->input->get('idcar'), FILTER_SANITIZE_STRING);
        

		$carMaker = $this->advisorysession['referenz']['carmaker'];
		$carModel = $this->advisorysession['referenz']['carmodel'];
		$carBodyType = $this->advisorysession['referenz']['carbodytype'];
		$carGeneration = $this->advisorysession['referenz']['cargeneration'];
		$carSelected = $this->advisorysession['referenz']['carselected'];
		
		

		$dataarray =  @$this->designmodel->getCar($this->advisorysession['wunschauto'], $idcar, $this->advisorysession['uiPrice'], $this->advisorysession['uiPassenger'], TRUE);
		$similar = $this->resultmodel->getSimilar($dataarray);
		// $bodytypes = $this->resultmodel->getSimilar($dataarray, $dataarray['carInformation']->engine);
		$bodytypes = $this->resultmodel->getSimilarBodyType($dataarray);
		// print_r($dataarray);

		// mehr-informationen.phtml
		$display = array(
			'page-title' => 'Advisory Process', // <title>
			'what-process' =>  'Dein Resultat', // breadcrumbs, h2
			'data-results' => $dataarray,
			'similar' => $similar,
			'bodytypes' => $bodytypes,
			'what-nav' => 0,
			'what-step' => 5,
			'active-page' => ''// <nav>
		);

		$this->load->view(
        'templates/datenbank/template-car-details.phtml', array(
            'display' => $display,
            'view' => 'templates/datenbank/mehr-informationen',
            'viewjs' => 'templates/advisoryprocess/mehr-informationen-js'
        )); 
	}
}