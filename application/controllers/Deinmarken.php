<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deinmarken extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->advisorysession = $this->session->userdata('advisorysession');
		$this->errorsession = $this->session->userdata('errorsession');
		$this->load->model('marken_model', 'markenmodel');

		if (!isset($this->advisorysession['uiPrice']) || (empty($this->advisorysession['uiPassenger']))) {
			redirect(base_url('deinegrundlagen'), 'refresh');
		}

		if (!empty($this->advisorysession['referenz']['carmaker']) &&
			!empty($this->advisorysession['referenz']['carmodel']) &&
			!empty($this->advisorysession['referenz']['carbodytype']) &&
			!empty($this->advisorysession['referenz']['cargeneration']) &&
			!empty($this->advisorysession['referenz']['carselected'])) {
			$this->arrayData = $this->advisorysession;
		} else {
			redirect(base_url('deinereferenz'), 'refresh');
		}

		if (!isset($this->advisorysession['wunschauto'])) {
			redirect(base_url('deinwunschauto'), 'refresh');	
		}
		
		if (!empty($this->advisorysession['deindesign']['cars'])) {
			redirect(base_url('deindesign'), 'refresh');
		}

		if ((count($this->advisorysession['deindesign']['selectedcars'])==0) ) {
			redirect(base_url('deinwunschauto'), 'refresh');
		}
	}

	
	public function index()
	{
		$error = false;
		$errorMessage = '';
		$arrayForMakeOnly = array();
		$arrayData = $this->arrayData;
		$logoResult = array();
		# bungkagon nako ang array og push og simple nga array
		if (count($arrayData['deindesign']['selectedcars']) > 0) {
			foreach ($arrayData['deindesign']['selectedcars'] as  $value) {
				array_push($arrayForMakeOnly, strtolower($value['carInformation']->make));
			}
		}

		# retrieve the logos
		# then match the logo if it's part of the selected cars, highlight (opacity 1) if one of the list.
		$arrayData['marken']['logos'] = $this->markenmodel->getLogo($arrayForMakeOnly);

		if ($_POST) {
			$arrayData['marken']['selectedlogos'] = array();
			foreach ($arrayData['marken']['logos'] as $value) {
				$this->form_validation->set_rules($value['logoInformation']->idcar_makes , 'Checkbox for'.$value['logoInformation']->maker, 'trim|required');
				$postResult = filter_var($this->input->post($value['logoInformation']->idcar_makes, FILTER_SANITIZE_STRING));

				//checkbox-strtolower($nameString[0])
				if ($postResult == true) {
					array_push($arrayData['marken']['selectedlogos'], $value['logoInformation']->idcar_makes);
				}
			}

			if ($this->form_validation->run() == FALSE) {
        		$error = true;
				$errorMessage = 'Please check for error';
				$errorsession = validation_errors(); 
				$this->session->unset_userdata('errorsession');
				$this->session->set_userdata('errorsession', $errorsession);
			} else {
				// $error = FALSE;
				$this->session->unset_userdata('advisorysession');
				$this->session->set_userdata('advisorysession', $arrayData);
				redirect(base_url('deinmarken/just3result'), 'refresh');
			}

		} else {
		
			$arraySelectedCar = array();
			$display = array(
				'page-title' => 'Advisory Process', // <title>
				'what-process' =>  'Deine Marken', // breadcrumbs, h2
				'what-nav' => 0,
				'what-step' => 3.5,
				'active-page' => '', // <nav>
				'arrayData' => $arrayData
			);

			$this->load->view(
	        'templates/advisoryprocess/template.phtml', array(
	            'display' => $display,
	            'view' => 'templates/advisoryprocess/dein-marken',
	            'viewjs' => 'templates/advisoryprocess/dein-marken-js'
	        )); 

	        $this->lang->line('welcome_message');
	    }

	}

	/* First display of result should only display 3*/
	public function just3result()
	{
		$arrayNew = array();
		$counter = 1;

		if (count($this->advisorysession['deindesign']['selectedcars']) > 1) {
			foreach ($this->advisorysession['deindesign']['selectedcars'] as $value) {
				if ($counter != 4) {
					array_push($arrayNew, $value);
					$counter++;
				}
				else {
					break;
				}
			}
			$this->advisorysession['deindesign']['selectedcars'] = $arrayNew;
		}

		$this->session->unset_userdata('advisorysession');
		$this->session->set_userdata('advisorysession', $this->advisorysession);
		redirect(base_url('deinresultat'), 'refresh');



		
		
	}


}