<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Underconstruction extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() 
	{
		parent::__construct();
		
	}

	
	public function index()
	{
		$display = array(
			// 'client-short-name' => 'NARS',
			// 'active-page' => 'messages'
			'page-title' => 'Advisory Process'
		);

		$this->load->view(
        'templates/under-construction.phtml', array(
            'display' => $display,
            'view' => 'content/dashboard'
        )); 

        // $this->lang->line('welcome_message');

	}

	public function test()
	{
		echo '15Qz0ZimOnNp+FHhKSfP/Lqt0JUdLG9nGJHCNd+YYYE/Zv9GRAr9IER1haL7XhItfUzivwjCkFQ0vSU/it1ziw==';
	}
}
