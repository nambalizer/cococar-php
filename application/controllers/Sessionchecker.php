<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessionchecker extends CI_Controller {
	public function __construct() 
	{
		parent::__construct();		
	}

	public function index()
	{
		$errorsession = $this->session->userdata('errorsession');
		$advisorysession = $this->session->userdata('advisorysession');

        print_r ($advisorysession);
        echo '<br>';
        echo '<br>';
        echo 'errorsession: ';
        print_r($errorsession);
        echo '<br>';
        echo 'json: <br><br><br>';
        echo json_encode($advisorysession);

        echo '<a href="'.base_url().'sessionchecker/clear">clear session</a>';
	}

	public function clear()
	{
		$this->session->unset_userdata('advisorysession');
		$this->session->unset_userdata('errorsession');

		redirect(base_url('sessionchecker'), 'refresh');
	}

	public function selectedcars()
	{
		$advisorysession = $this->session->userdata('advisorysession');

		foreach ($advisorysession['deindesign']['cars'] as $value) {
			print_r($value);
			echo '<br><br>';
		}
	}

	public function pop10cars()
	{
		$advisorysession = $this->session->userdata('advisorysession');
		$newArray = array();

		if ($advisorysession['deindesign']['cars'] > 5)
		{
			$counter = 5;
			while($counter >= 1) {
				$fromArrray = array_pop($advisorysession['deindesign']['cars']);	
				array_push($newArray, $fromArrray);
				$counter--;
			}
			
		}

		if (isset($newArray)) {
			usort($newArray, $this->build_sorter('totalCoefficient'));
		}

		foreach ($newArray as $value) {
			print_r($value);
			echo '<br><br>';
		}
	}

	private function build_sorter($key) {
	    return function ($a, $b) use ($key) {
	    	if($a[$key]==$b[$key]) return 0;
    			return $a[$key] > $b[$key]?1:-1;
	    	// return $a[$key] < $b[$key] 
	     //    return strnatcmp($a[$key], $b[$key]);
	    };
	}
}